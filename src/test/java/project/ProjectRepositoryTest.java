package project;

import static org.junit.Assert.*;

import java.sql.Date;
import java.util.Collection;
import java.util.NoSuchElementException;

import org.junit.*;
import org.junit.rules.ExpectedException;

import ch.briggen.bfh.sparklist.SparkListServer;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.repositories.CustomerRepository;
import ch.briggen.bfh.sparklist.repositories.PortfolioRepository;
import ch.briggen.bfh.sparklist.repositories.ProjectPhaseRepository;
import ch.briggen.bfh.sparklist.repositories.ProjectRepository;
import ch.briggen.bfh.sparklist.repositories.ProjectRiskRepository;

/*
 * TestClass for the ProjectRepository
 */
public class ProjectRepositoryTest {
	static SparkListServer server;
	
	@Rule
    public ExpectedException thrown= ExpectedException.none();
	
	/*
	 * Is called at startup of this UnitTestClass. Sets up server
	 */
	@BeforeClass
	public static void startupServer(){
		server = new SparkListServer();
    	server.configure();
    	server.run();
	}
	
	/*
	 * Is called before every Unit Test in this class. Reinitializes the database
	 */
	@Before
	public void initDb(){
		server.doInitializeDB();
	}

	@Test
	public void testGetAll() {
		ProjectRepository repo = new ProjectRepository();
		Collection<Project> projects = repo.getAll();
		
		assertEquals(5, projects.size());
	}
	
	@Test
	public void testGetById() {
		ProjectRepository repo = new ProjectRepository();
		Project p = repo.getById(1);
		
		assertTrue(p != null);
		assertEquals("Boss-Pet", p.getName());
	}
	
	@Test
	public void testGetById_wrongId() {
		thrown.expect(NoSuchElementException.class);
		
		ProjectRepository repo = new ProjectRepository();		
		repo.getById(99);
	}

	@Test
	public void testGetByPortfolio() {
		ProjectRepository repo = new ProjectRepository();
		Collection<Project> projects = repo.getByPortfolio(1);
		
		assertEquals(3, projects.size());
	}

	@Test
	public void testUpdate() {
		String newName = "TestProjektName";
		ProjectRepository repo = new ProjectRepository();
		
		//get project to update
		Project p = repo.getById(1);
		p.setName(newName);
		
		//save update & get it again
		repo.update(p);
		Project p2 = repo.getById(1);
		
		assertEquals(newName, p2.getName());
	}

	@Test
	public void testDelete() {
		thrown.expect(NoSuchElementException.class);
		long id = 1;
		ProjectRepository repo = new ProjectRepository();
		repo.delete(id);
		Project p = repo.getById(id);
		
		assertEquals(id, p.getId());
	}
	
	@Test
	public void testDelete_wrongId() {
		ProjectRepository repo = new ProjectRepository();
		repo.delete(99);
		
		// no assert because no return value.
		// no exception = success
	}

	/*
	 * This Test is written very bad. Multiple dependencies and so on
	 */
	@Test
	public void testInsert() {
		PortfolioRepository portfolioRepo = new PortfolioRepository();
		CustomerRepository customerRepo = new CustomerRepository();
		ProjectRiskRepository riskRepo = new ProjectRiskRepository();
		ProjectPhaseRepository phaseRepo = new ProjectPhaseRepository();
		ProjectRepository repo = new ProjectRepository();
		
		Project p = new Project();
		p.setIsActive(1);
		p.setName("testproject");
		p.setStartDate(new Date(0));
		p.setEndDate(new Date(0));
		p.setBudget(0);
		p.setActualCost(0);
		p.setPlannedValue(0);
		p.setPortfolio(portfolioRepo.getById(1));
		p.setCustomer(customerRepo.getById(1));
		p.setRisks(riskRepo.getById(1));
		p.setProjectPhase(phaseRepo.getById(1));
		
		long id = repo.insert(p);
		Project p2 = repo.getById(id);
		
		assertEquals(p.getName(), p2.getName());
	}

}
