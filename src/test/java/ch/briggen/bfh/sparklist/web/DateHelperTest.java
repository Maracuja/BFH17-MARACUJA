package ch.briggen.bfh.sparklist.web;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

import ch.briggen.bfh.sparklist.DateHelper;

public class DateHelperTest {

	@Test
	public void testStringToDate() {
		String s = "2017-05-12";
		assertTrue(DateHelper.StringToDate(s) != null);
	}
	@Test
	public void testStringToDate_Fail() {
		String s = "";
		assertTrue(DateHelper.StringToDate(s) == null);
	}

	@Test
	public void testStringToSqlDate() {
		String s = "2017-05-12";
		assertTrue(DateHelper.StringToSqlDate(s) != null);
	}
	@Test
	public void testStringToSqlDate_Fail() {
		String s = "";
		assertTrue(DateHelper.StringToSqlDate(s) == null);
	}

	@Test
	public void testDateToSqlDate() {
		Date d = new Date();
		assertTrue(DateHelper.DateToSqlDate(d) != null);
	}
}
