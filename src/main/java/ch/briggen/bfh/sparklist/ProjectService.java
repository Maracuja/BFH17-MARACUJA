package ch.briggen.bfh.sparklist;

import java.text.DecimalFormat;
import java.util.Date;

import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.repositories.CustomerRepository;
import ch.briggen.bfh.sparklist.repositories.EmployeeRepository;
import ch.briggen.bfh.sparklist.repositories.ProjectRepository;

/*
 * ProjectService class serves as an API for project-data and -stats.
 * Has endpoints for chart-data and more
 */
public class ProjectService {
	// project-obj to get data from
	private Project project;
	
	//constructor. Initializes a repo and gets the project from the db
	public ProjectService(long id) {
        ProjectRepository projectRepo = new ProjectRepository();
        project = projectRepo.getById(id);
	}
	
	/*
	 * Uses the start- and end-date of a project to calculate how much time has passed and how much time is left
	 *  returns a pair of doubles [20,80] indicating in percent [timePassed, timeLeft]
	 *  is called via AJAX from frontend
	 */
	public Object getTimeProgressChart() {
		//calculate progress of project in days because "The JDK Date API is horribly broken" -__- http://stackoverflow.com/questions/1555262/calculating-the-difference-between-two-java-date-instances
		double startDays = project.getStartDate().getTime() / (24 * 60 * 60 * 1000);
		double endDays = project.getEndDate().getTime() / (24 * 60 * 60 * 1000);
		double currentDays = new Date().getTime() / (24 * 60 * 60 * 1000);
		
		double totalDays = endDays - startDays + 1;
		double usedDays = currentDays - startDays;
		
		//calculate percents of days passed and days left in project
		double temp = 100 / totalDays;
		double progress = temp * usedDays;
		double diff = 100 - progress;
		
		// if wer're past the end date of a project, just return 100%
		if(progress > 100 || diff > 100) {
			progress = 100;
			diff = 0;
		}
		if(diff > 100) {
			progress = 0;
			diff = 100;			
		}
		
		//format result as a pair of doubles so the frontend-chart can use it 
		double[] data = {Double.parseDouble(new DecimalFormat("##").format(progress)), Double.parseDouble(new DecimalFormat("##").format(diff))};
		
		return data;
	}

	/*
	 * Returns the budget and actualCost values of a project
	 */
	public Object getBudgetProgressChart() {
		double[] data = {project.getBudget(), project.getActualCost()};
		return data;
	}

	/*
	 * Returns all customers
	 */
	public Object getCustomers() {
		CustomerRepository customerRepo = new CustomerRepository();

		return customerRepo.getAll();
	}

	/*
	 * Returns all employees
	 */
	public Object getEmployee() {
		EmployeeRepository employeeRepo = new EmployeeRepository();

		return employeeRepo.getAll();
	}	
}