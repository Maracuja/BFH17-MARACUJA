package ch.briggen.bfh.sparklist.repositories;

import static ch.briggen.bfh.sparklist.repositories.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Portfolio;
import ch.briggen.bfh.sparklist.domain.Project;

/**
 * Repository for the portfolio
 * DB operations to the various configuration tables
 * @author Tenud 
 *
 */
public class PortfolioRepository {
	
private final Logger log = LoggerFactory.getLogger(PortfolioRepository.class);
	
	/**
	 * Get all portfolios in DB
	 * @return Collection of all portfolios
	 */
	public Collection<Portfolio> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM Portfolio WHERE isActive=1");
			ResultSet rs = stmt.executeQuery();
			return mapPortfolio(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all portfolios. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Gets all portfolios with given name
	 * @param name name to filter by
	 * @return Collection with name "name"
	 */
	public Collection<Portfolio> getByName(String name) {
		log.trace("getByName " + name);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM Portfolio WHERE NAME=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			return mapPortfolio(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving portfolio by name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Gets the portfolio with the given ID
	 * @param id id of the portfolio
	 * @return Portfolio or NULL
	 */
	public Portfolio getById(long id) {
		log.trace("getById " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM Portfolio WHERE pk_idPortfolio=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapPortfolio(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving portfolio by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}
	
	/**
	 * counts the number of projects assigned to a certain portfolio
	 * @param id the id of the portfolio to count projects in
	 * @return the number of projects
	 */
	public int countProjects(long id)
	{
		log.trace("getById " + id);
		int  size = 0;
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM PROJECT WHERE fk_Portfolio=? AND isActive=1");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			rs.last();
		    size = rs.getRow();
			return size;		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving project by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Updates the passed portfolio in DB
	 * @param p the portfolio to update
	 * @return the id of the saved portfolio
	 */
	public long update(Portfolio p) {
		log.trace("update " + p);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("UPDATE Portfolio SET ISACTIVE =?, NAME=?, DESCRIPTION=? WHERE pk_idPortfolio=?");
			stmt.setInt(1, p.getIsActive());
			stmt.setString(2, p.getName());
			stmt.setString(3, p.getDescription());
			stmt.setLong(4, p.getId());
			stmt.executeUpdate();

			return p.getId();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating portfolio " + p;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Delete portfolio with the given ID
	 * @param id Portfolio ID
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("DELETE FROM Portfolio WHERE pk_idPortfolio=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing portfolio by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Saves the given Portfolio in DB
	 * @param p new Portfolio
	 * @return Returns the new ID of the created portfolio
	 */
	public long insert(Portfolio p) {
		
		log.trace("insert " + p);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("INSERT INTO Portfolio (IsActive, Name, Description) values (?,?,?)");
			stmt.setInt(1, 1);
			stmt.setString(2, p.getName());
			stmt.setString(3, p.getDescription());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while adding portfolio " + p;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper to convert the results to portfolio-objects. 
	 * @author Tenud Chris
	 * @throws SQLException 
	 *
	 */
	private static Collection<Portfolio> mapPortfolio(ResultSet rs) throws SQLException 
	{
		LinkedList<Portfolio> list = new LinkedList<Portfolio>();
		while(rs.next())
		{
			Portfolio p = new Portfolio(
					rs.getLong("pk_idPortfolio"),
					rs.getInt("IsActive"),
					rs.getString("Name"),
					rs.getString("Description"));
			list.add(p);
		}
		return list;
	}
	
	/**
	 * Get all Budgets and aggregate them to a total
	 * @return Integer budget
	 */
	public long getAllBudgets(long id) {
		log.trace("budgets ");
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT SUM(BUDGET) as budget FROM Project WHERE ISACTIVE = 1 AND FK_PORTFOLIO =?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			rs.first();	
			return rs.getLong("BUDGET");	
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving budgets ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Get all Costs and aggregate them to a total
	 * @return Integer costs
	 */
	public long getAllCosts(long id) {
		log.trace("costs ");
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT SUM(ACTUALCOST) as costs FROM Project WHERE ISACTIVE = 1 AND FK_PORTFOLIO =?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			rs.first();	
			return rs.getLong("costs");
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving costs ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Get state of the portfolio
	 * @param id of the portfolio
	 * @return String status
	 */
	public String calculatePortfolioStatus(long id) {
		log.trace("getById " + id);
	    int counter = 0;
	    double avg = 0.0;
		ProjectRepository projectRepo = new ProjectRepository();
		Collection<Project> projects = projectRepo.getByPortfolio(id);
		
	    for (Project project : projects) {
	    	switch(project.getStatus())
			{
				case "green":
					avg += 1;
					break;
				case "yellow":
					avg += 2;
					break;
				case "red":
					avg += 3;
					break;
				case "all":
				case "end":
				default:
					break;
			}
	    	counter++;
	    }
	    int result =  (int) Math.ceil(avg / counter);
	    
	    switch(result)
	    {
	    case 0:
		    	if(counter == 0)
		    	{
		    		return "all";
		    	}
	    		return "endPortfolio";
		    case 1:
		    	return "green";
		    case 2:
		    	return "yellow";
		    case 3:
		    	return "red";
		    default:
		    	return "all";
	    }
	}

	/**
	 * Get state of the portfolio
	 * @param id of the portfolio
	 * @return int risk level
	 */
	public Object calculatePortfolioRisks(Long id) {
	    double avg = 0.0;
	    int counter = 0;
		ProjectRepository projectRepo = new ProjectRepository();
		Collection<Project> projects = projectRepo.getByPortfolio(id);    
	    
	    for (Project project : projects) {	    	
	    	avg += project.getRisks().getId();
	    	counter++;
	    }
	    int result =  (int) Math.round(avg / counter);
	    
	    if(result == 0) {
	    	return "all";
	    }
	    return result;
	}
}
