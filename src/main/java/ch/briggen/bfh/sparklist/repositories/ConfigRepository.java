/**
 * 
 */
package ch.briggen.bfh.sparklist.repositories;

import static ch.briggen.bfh.sparklist.repositories.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.DateHelper;
import ch.briggen.bfh.sparklist.domain.Config;

/**
 * Repository for the configuration
 * DB operations to the various configuration tables
 * @author Tenud 
 *
 */
public class ConfigRepository {
	
private final Logger log = LoggerFactory.getLogger(PortfolioRepository.class);

	/**
	 * Gets the config with the given ID
	 * @param id id of the config
	 * @return config or NULL
	 */
	public Config getById(int id) {
		log.trace("getById " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM CONFIGURATION WHERE PK_IDCONFIGURATION=?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapConfig(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving config by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Saves the given config in DB
	 * @param c the updated config to save
	 * @return Returns the  ID of the saved configuration
	 */
	public long update(Config c) {
		log.trace("update " + c);
		
		try(Connection conn = getConnection())
		{

			java.sql.Date expirydate = DateHelper.DateToSqlDate(c.getExpiryDate());
			PreparedStatement stmt = conn.prepareStatement("UPDATE CONFIGURATION SET "
																			+ "companyname=?, "
																			+ "adress=?, "
																			+ "zipcode=?, "
																			+ "state=?, "
																			+ "licencekey=?, "
																			+ "expirydate=? "
																			+ " WHERE PK_IDCONFIGURATION=?");	
			stmt.setString(1, c.getCompanyName());
			stmt.setString(2, c.getAdress());
			stmt.setString(3, c.getZipCode());
			stmt.setString(4, c.getState());
			stmt.setString(5, c.getLicenceKey());
			stmt.setDate(6, expirydate);
			stmt.setInt(7, c.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating configuration " + c;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
		return c.getId();		
	}

	/**
	 * Delete config with the given ID
	 * @param id Config ID
	 */
	public void delete(int id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("DELETE FROM CONFIGURATION WHERE pk_idConfiguration=?");
			stmt.setInt(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "Cannot delete this configuration. Configuration is still in use by a portfolio";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Saves the given Configuration in DB
	 * @param c new Configuration
	 * @return Returns the new ID of the created configuration
	 */
	public long insert(Config c) {
		
		log.trace("insert " + c);
		int nextId;
		
		try(Connection conn = getConnection())
		{

			java.sql.Date expirydate = DateHelper.DateToSqlDate(c.getExpiryDate());
			PreparedStatement stmt2 = conn.prepareStatement("SELECT MAX(pk_idconfiguration)+1 as maxId FROM CONFIGURATION");
			stmt2.executeQuery();
			nextId = stmt2.getResultSet().getInt(1);
			
			/* TODO: check expiry date correct syntax! */
			PreparedStatement stmt = conn.prepareStatement("INSERT INTO CONFIGURATION VALUES ("
																			+ "?, " /* pk_idconfiguration */
																			+ "?, " /* companyname */
																			+ "?, " /* adress */
																			+ "?, " /* zipcode */
																			+ "?, " /* state */
																			+ "?, " /* licencekey */
																			+ "?)" /* expirydate */
																		);
			stmt.setInt(1, nextId);
			stmt.setString(2, c.getCompanyName());
			stmt.setString(3, c.getAdress());
			stmt.setString(4, c.getZipCode());
			stmt.setString(5, c.getState());
			stmt.setString(6, c.getLicenceKey());
			stmt.setDate(7, expirydate);
			stmt.executeUpdate();
			
			return nextId;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while adding configuration " + c;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper to convert the result of db queries to configuration-objects. 
	 * @param rs the result of the db query
	 * @return a list of configuration-objects
	 * @throws SQLException 
	 */
	private static Collection<Config> mapConfig(ResultSet rs) throws SQLException 
	{
		LinkedList<Config> list = new LinkedList<Config>();
		while(rs.next())
		{
			Config c = new Config(rs.getInt("pk_idConfiguration"),rs.getString("companyname"),rs.getString("adress"),rs.getString("zipcode"),rs.getString("state"),rs.getString("licencekey"), rs.getDate("expirydate"));
			list.add(c);
		}
		return list;
	}

}
