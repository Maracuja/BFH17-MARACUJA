package ch.briggen.bfh.sparklist.repositories;
import static ch.briggen.bfh.sparklist.repositories.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Employee;
/**
 * Repository for employee. 
 * DB operations for the employee class
 * @author Tobias Luethi
 *
 */
public class EmployeeRepository {
	
	private final Logger log = LoggerFactory.getLogger(EmployeeRepository.class);
	/**
	 * get all employee elements
	 * @return Collection of employee
	 */
	public Collection<Employee> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM Employee");
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all items. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * return employee by id
	 * @param id id of the employee
	 * @return employee or null
	 */
	public Employee getById(long id) {
		log.trace("getById " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM Employee WHERE pk_idEmployee=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving customer by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}
	/**
	 * update employee element
	 * @param employee object to save
	 */
	public void update(Employee employee) {
		log.trace("update " + employee);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("UPDATE Employee SET "
																	+ "firstname=?, "
																	+ "lastname=?, "
																	+ "section=?, "
																	+ "function=?, "
																	+ "rate=?"
																	+ "WHERE pk_idEmployee=?");			
			stmt.setString(1, employee.getFirstname());
			stmt.setString(2, employee.getLastname());
			stmt.setString(3, employee.getSection());
			stmt.setString(4, employee.getFunction());
			stmt.setBigDecimal(5, employee.getRate());
			stmt.setLong(6, employee.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating item " + employee;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}
	/**
	 * delete employee elements
	 * @param id employee id
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("DELETE FROM Employee WHERE pk_idEmployee=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "Customer can not be deleted. The customer is still used in a project";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}
	/**
	 * save customer into the database
	 * @param customer customer class
	 * @return id id of the customer db entry
	 */
	public long insert(Employee employee) {
		
		log.trace("insert " + employee);
		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		int nextId;
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt2 = conn.prepareStatement("SELECT MAX(pk_idEmployee)+1 as maxId FROM Employee");
			stmt2.executeQuery();
			stmt2.getResultSet().next();
			nextId = stmt2.getResultSet().getInt(1);
			PreparedStatement stmt = conn.prepareStatement("INSERT INTO Employee VALUES("
															+ "?, " /* pk_idEmployee */
															+ "?, " /* firstname */
															+ "?, " /* lastname */
															+ "?, " /* section */
															+ "?, " /* function */
															+ "?)" /* rate */
														);
			stmt.setInt(1, nextId);
			stmt.setString(2, employee.getFirstname());
			stmt.setString(3, employee.getLastname());
			stmt.setString(4, employee.getSection());
			stmt.setString(5, employee.getFunction());
			stmt.setBigDecimal(6, employee.getRate());		
			stmt.executeUpdate();
			
			return nextId;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating item " + employee;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Helper to convert sql return in employee class
	 * @param rs the result of the db query
	 * @return a list of employee-objects
	 * @author Tobias Luethi
	 * @throws SQLException 
	 *
	 */
	private static Collection<Employee> mapItems(ResultSet rs) throws SQLException 
	{
		LinkedList<Employee> list = new LinkedList<Employee>();
		
		while(rs.next())
		{
			Employee employee = new Employee(rs.getInt("pk_idEmployee"),
									rs.getString("firstname"),
									rs.getString("lastname"),
									rs.getString("section"),
									rs.getString("function"),
									rs.getBigDecimal("rate")
								);
			list.add(employee);
		}
		return list;
	}
}