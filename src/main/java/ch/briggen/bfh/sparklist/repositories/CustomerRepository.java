package ch.briggen.bfh.sparklist.repositories;
import static ch.briggen.bfh.sparklist.repositories.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Customer;
/**
 * Repository for customers. 
 * DB operations for the customer class
 * @author Tobias Luethi
 *
 */
public class CustomerRepository {
	
	private final Logger log = LoggerFactory.getLogger(CustomerRepository.class);
	/**
	 * get all customer elements
	 * @return Collection of customers
	 */
	public Collection<Customer> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM CUSTOMER");
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all items. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	/**
	 * return collection of customers with the same name
	 * @param name the name to use as filter
	 * @return Collection of customer with the name "name"
	 */
	public Collection<Customer> getByName(String name) {
		log.trace("getByName " + name);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM CUSTOMER WHERE name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving customer by name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}
	/**
	 * return customer by id
	 * @param id id of the customer
	 * @return customer or null
	 */
	public Customer getById(long id) {
		log.trace("getById " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM CUSTOMER WHERE pk_idcustomer=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving customer by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}
	/**
	 * update customer object
	 * @param customer to save
	 */
	public void update(Customer customer) {
		log.trace("update " + customer);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("UPDATE CUSTOMER SET "
																	+ "name=?, "
																	+ "firstname=?, "
																	+ "adress=?, "
																	+ "phone=?, "
																	+ "email=?, "
																	+ "company=?, "
																	+ "state=?, "
																	+ "zipcode=?"
																	+ "WHERE pk_idcustomer=?");			
			stmt.setString(1, customer.getName());
			stmt.setString(2, customer.getFirstname());
			stmt.setString(3, customer.getAdress());
			stmt.setString(4, customer.getPhone());
			stmt.setString(5, customer.getEmail());
			stmt.setString(6, customer.getCompany());
			stmt.setString(7, customer.getState());
			stmt.setString(8, customer.getZipCode());	
			stmt.setLong(9, customer.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating item " + customer;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}
	/**
	 * delete customer object
	 * @param id customer id
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("DELETE FROM CUSTOMER WHERE pk_idcustomer=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "Customer can not be deleted. The customer is still used in a project";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}
	/**
	 * save customer into the database
	 * @param customer customer class
	 * @return id id of the customer db entry
	 */
	public long insert(Customer customer) {
		
		log.trace("insert " + customer);
		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		int nextId;
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt2 = conn.prepareStatement("SELECT MAX(pk_idcustomer)+1 as maxId FROM CUSTOMER");
			stmt2.executeQuery();
			stmt2.getResultSet().next();
			nextId = stmt2.getResultSet().getInt(1);
			PreparedStatement stmt = conn.prepareStatement("INSERT INTO CUSTOMER VALUES("
															+ "?, " /* pk_idcustomer */
															+ "?, " /* name */
															+ "?, " /* firstname */
															+ "?, " /* adress*/
															+ "?, " /* phone */
															+ "?, " /* email */
															+ "?, " /* company */
															+ "?, " /* state */
															+ "?)"  /* zipcode */
														);
			stmt.setInt(1, nextId);
			stmt.setString(2, customer.getName());
			stmt.setString(3, customer.getFirstname());
			stmt.setString(4, customer.getAdress());
			stmt.setString(5, customer.getPhone());
			stmt.setString(6, customer.getEmail());
			stmt.setString(7, customer.getCompany());
			stmt.setString(8, customer.getState());
			stmt.setString(9, customer.getZipCode());			
			stmt.executeUpdate();
			
			return nextId;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating item " + customer;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Helper to convert sql return in customer class
	 * @param rs the result of the db query
	 * @return a list of customer-objects
	 * @author Tobias Luethi
	 * @throws SQLException 
	 *
	 */
	private static Collection<Customer> mapItems(ResultSet rs) throws SQLException 
	{
		LinkedList<Customer> list = new LinkedList<Customer>();
		
		while(rs.next())
		{
			Customer customer = new Customer(rs.getLong("pk_idCustomer"),
									rs.getString("name"),
									rs.getString("firstname"),
									rs.getString("adress"),
									rs.getString("phone"),
									rs.getString("email"),
									rs.getString("company"),
									rs.getString("state"),
									rs.getString("zipcode")
								);
			list.add(customer);
		}
		return list;
	}
}