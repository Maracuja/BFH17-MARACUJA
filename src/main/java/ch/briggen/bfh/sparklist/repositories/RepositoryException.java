package ch.briggen.bfh.sparklist.repositories;

public class RepositoryException extends RuntimeException {
	/**
	 * added this to remove warning
	 */
	private static final long serialVersionUID = 1L;

	public RepositoryException(String message) {
		super(message);
	}

}
