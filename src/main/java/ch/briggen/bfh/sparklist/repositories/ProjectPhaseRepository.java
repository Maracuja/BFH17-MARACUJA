package ch.briggen.bfh.sparklist.repositories;

import static ch.briggen.bfh.sparklist.repositories.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ProjectPhase;

/**
 * Repository for projectphases. 
 * DB operations for the ProjectPhase class
 *
 */
public class ProjectPhaseRepository {
	
	private final Logger log = LoggerFactory.getLogger(ProjectRiskRepository.class);
	
	/**
	 * Gets the status with the given ID
	 * @param id id of the status
	 * @return status or NULL
	 */
	public ProjectPhase getById(int id) {
		log.trace("getById " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM PROJECTPHASE WHERE pk_idPROJECTPHASE=?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapProjectPhase(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving status by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}
	
	/**
	 * Gets all available projectphases
	 * @return a collection of projectphases
	 */
	public Collection<ProjectPhase> getAllPhases() {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM PROJECTPHASE");
			ResultSet rs = stmt.executeQuery();
			return mapProjectPhase(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all items. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Helper to convert the results to status-objects. 
	 * @author Tobias L�thi
	 * @throws SQLException 
	 *
	 */
	private static Collection<ProjectPhase> mapProjectPhase(ResultSet rs) throws SQLException 
	{
		LinkedList<ProjectPhase> list = new LinkedList<ProjectPhase>();
		
		while(rs.next())
		{
			ProjectPhase projectPhase = new ProjectPhase(rs.getInt("pk_idPROJECTPHASE"),
														 rs.getString("PHASE")
														);
			list.add(projectPhase);
		}
		return list;
	}

}
