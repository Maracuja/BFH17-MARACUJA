package ch.briggen.bfh.sparklist.repositories;

import static ch.briggen.bfh.sparklist.repositories.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ProjectRisk;

/**
 * Repository for projectrisks. 
 * DB operations for the ProjectRisk class
 *
 */
public class ProjectRiskRepository {
	
	private final Logger log = LoggerFactory.getLogger(ProjectRiskRepository.class);
	
	/**
	 * Gets the status with the given ID
	 * @param id id of the status
	 * @return status or NULL
	 */
	public ProjectRisk getById(int id) {
		log.trace("getById " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM RISKCLASS WHERE pk_idRISK=?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapProjectRisk(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving status by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}
	
	/**
	 * Gets all available projectrisks
	 * @return a collection of projectrisks
	 */
	public Collection<ProjectRisk> getAllRisks() {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM RISKCLASS");
			ResultSet rs = stmt.executeQuery();
			return mapProjectRisk(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all items. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Helper to convert the results to status-objects. 
	 * @author Tobias L�thi
	 * @throws SQLException 
	 *
	 */
	private static Collection<ProjectRisk> mapProjectRisk(ResultSet rs) throws SQLException 
	{
		LinkedList<ProjectRisk> list = new LinkedList<ProjectRisk>();
		
		while(rs.next())
		{
			ProjectRisk ProjectRisk = new ProjectRisk(rs.getInt("pk_idRISK"),
									rs.getString("TEXT")
								);
			list.add(ProjectRisk);
		}
		return list;
	}

}
