package ch.briggen.bfh.sparklist.repositories;
import static ch.briggen.bfh.sparklist.repositories.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.DateHelper;
import ch.briggen.bfh.sparklist.domain.Customer;
import ch.briggen.bfh.sparklist.domain.Portfolio;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectPhase;
import ch.briggen.bfh.sparklist.domain.ProjectRisk;
/**
 * Repository for projects. 
 * DB operations for the project class
 * @author Tobias Luethi
 *
 */
public class ProjectRepository {
	
	private final Logger log = LoggerFactory.getLogger(ProjectRepository.class);
	private PortfolioRepository portfolioRepo = new PortfolioRepository();
	private CustomerRepository customerRepo = new CustomerRepository();
	private ProjectRiskRepository riskRepo = new ProjectRiskRepository();
	private ProjectPhaseRepository phaseRepo = new ProjectPhaseRepository();
	
	/**
	 * get all project elements
	 * @return Collection of projects
	 */
	public Collection<Project> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM PROJECT WHERE isActive=1");
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all items. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	/**
	 * return project by id
	 * @param id id of the project
	 * @return project or null
	 */
	public Project getById(long id) {
		log.trace("getById " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM PROJECT WHERE pk_idProject=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving project by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}
	/**
	 * return projects by portfolio-id
	 * @param id id of the portfolio
	 * @return projects or null
	 */
	public Collection<Project> getByPortfolio(long id) {
		log.trace("getById " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM PROJECT WHERE fk_Portfolio=? AND isActive=1");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving project by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}
	/**
	 * update project element
	 * @param project
	 */
	public long update(Project project) {
		log.trace("save " + project);
		
		java.sql.Date sqlStartDate = DateHelper.DateToSqlDate(project.getStartDate());
		java.sql.Date sqlEndDate = DateHelper.DateToSqlDate(project.getEndDate());
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("UPDATE PROJECT SET "
																	+ "isactive=?, "
																	+ "name=?, "
																	+ "owner=?, "
																	+ "budget=?, "
																	+ "plannedvalue=?, "
																	+ "actualcost=?, "
																	+ "milestones=?,"
																	+ "startdate=?,"
																	+ "enddate=?,"
																	+ "stakeholder=?,"
																	+ "description=?,"
																	+ "fk_customer=?,"
																	+ "fk_portfolio=?,"
																	+ "FK_RISKCLASS=?,"
																	+ "FK_PROJECTPHASE=?"
																	+ " WHERE pk_idProject=?");			
			stmt.setInt(1, project.getIsActive());
			stmt.setString(2, project.getName());
			stmt.setString(3, project.getOwner());
			stmt.setInt(4, project.getBudget());
			stmt.setInt(5, project.getPlannedValue());
			stmt.setInt(6, project.getActualCost());
			stmt.setString(7, project.getMilestone());
			stmt.setDate(8, sqlStartDate);
			stmt.setDate(9, sqlEndDate);
			stmt.setString(10, project.getStakeholder());
			stmt.setString(11, project.getDescription());
			stmt.setInt(12, (int)project.getCustomer().getId());
			stmt.setInt(13, (int)project.getPortfolio().getId());
			stmt.setInt(14,  project.getRisks().getId());
			stmt.setInt(15, project.getProjectPhase().getId());
			stmt.setLong(16, project.getId());
			stmt.executeUpdate();
			
			return project.getId();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating item " + project;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}
	/**
	 * delete project elements
	 * @param id project id
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			//todo: delete relations/relating object that are now longer needed
			PreparedStatement stmt = conn.prepareStatement("DELETE FROM PROJECT WHERE pk_idProject=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing project by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}				
	}
	/**
	 * save project into the database
	 * @param project project class
	 * @return id id of the project db entry
	 */
	public long insert(Project project) {
		log.trace("insert " + project);
		
		try(Connection conn = getConnection())
		{
			//generate id by adding 1 to highest existing id in db
			int nextId;
			PreparedStatement stmt2 = conn.prepareStatement("SELECT MAX(pk_idproject)+1 as maxID FROM PROJECT");
			stmt2.executeQuery();
			stmt2.getResultSet().next();
			nextId = stmt2.getResultSet().getInt(1);
			System.out.println(nextId);
		
			PreparedStatement stmt = conn.prepareStatement("INSERT INTO PROJECT VALUES("
															+ "?, " /* pk_idProject */
															+ "?, " /* isactive */
															+ "?, " /* name */
															+ "?, " /* owner */
															+ "?, " /* FK_PROJECTPHASE */
															+ "?, " /* budget */
															+ "?, " /* plannedvalue */
															+ "?, " /* actualcost */
															+ "?, " /* milestones */
															+ "?, " /* startdate */
															+ "?, " /* enddate */
															+ "?, " /* stakeholder */
															+ "?, " /* description */
															+ "?, " /* fk_customer */
															+ "?, " /* fk_portfolio */														
															+ "?) " /* fk_projectrisk */
														);
			stmt.setLong(1, nextId);
			stmt.setInt(2, 1); //isActive = 1 for new projects
			stmt.setString(3, project.getName());
			stmt.setString(4, project.getOwner());
			stmt.setInt(5, project.getBudget());
			stmt.setInt(6, project.getPlannedValue());
			stmt.setInt(7, project.getActualCost());
			stmt.setString(8, project.getMilestone());
			stmt.setDate(9, DateHelper.DateToSqlDate(project.getStartDate()));
			stmt.setDate(10, DateHelper.DateToSqlDate(project.getEndDate()));
			stmt.setString(11, project.getStakeholder());
			stmt.setString(12, project.getDescription());
			stmt.setInt(13, (int)project.getCustomer().getId());
			stmt.setInt(14, (int)project.getPortfolio().getId());
			stmt.setInt(15,  project.getRisks().getId());
			stmt.setInt(16, project.getProjectPhase().getId());
			stmt.executeUpdate();
			
			return nextId;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating item " + project;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
	}
	
	public Collection<Integer> getEmployeesByProject(long projectId) {
		log.trace("getById " + projectId);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT fk_employee FROM PROJECT_EMPLOYEE WHERE FK_PROJECT=?");
			stmt.setLong(1, projectId);
			ResultSet rs = stmt.executeQuery();
			return mapEmployees(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving project employees by id " + projectId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * save project employee into the database
	 * @param project id
	 * @param emplyee id
	 */
	public void insertProjectEmployee(long projectId, int employeeId) {
		try(Connection conn = getConnection())
		{
			int nextId;
			PreparedStatement stmtEmployeeID = conn.prepareStatement("SELECT MAX(pk_idprojectemployee)+1 as maxID FROM PROJECT_EMPLOYEE");
			stmtEmployeeID.executeQuery();
			stmtEmployeeID.getResultSet().next();
			nextId = stmtEmployeeID.getResultSet().getInt(1);
			System.out.println(nextId);
		
			PreparedStatement stmtEmployee = conn.prepareStatement("INSERT INTO PROJECT_EMPLOYEE VALUES("
					+ "?, " /* pk_idProject */
					+ "?, " /* FK_EMPLOYEE */													
					+ "?) " /* FK_PROJECT */
				);
			stmtEmployee.setInt(1, nextId);
			stmtEmployee.setInt(2, employeeId);
			stmtEmployee.setLong(3, projectId);
			stmtEmployee.executeUpdate();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating employee in project_employee projectId:" + projectId + " EmployeeId:" + employeeId;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * delete project employee from the database
	 * @param project id
	 */
	public void DeleteProjectEmployee(long projectId) {
		log.trace("delete " + projectId);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("DELETE FROM PROJECT_EMPLOYEE WHERE FK_PROJECT=?");
			stmt.setLong(1, projectId);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing project employee by project id " + projectId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}	
	}
	
	
	/**
	 * Helper to convert sql return in project class
	 * @throws SQLException 
	 *
	 */
	private Collection<Project> mapItems(ResultSet rs) throws SQLException 
	{
		LinkedList<Project> list = new LinkedList<Project>();
		
		while(rs.next())
		{
			//get related objects from db
			Customer customer = customerRepo.getById(Long.parseLong(rs.getString("fk_customer")));
			Portfolio pf = portfolioRepo.getById(Long.parseLong(rs.getString("fk_portfolio")));
			ProjectRisk risk = riskRepo.getById(Integer.parseInt(rs.getString("fk_riskclass")));
			ProjectPhase phase = phaseRepo.getById(Integer.parseInt(rs.getString("FK_PROJECTPHASE")));
			
			Project project = new Project(rs.getLong("pk_idProject"),
									rs.getInt("isactive"),
									rs.getString("name"),
									rs.getString("owner"),
									rs.getInt("budget"),
									rs.getInt("plannedvalue"),
									rs.getInt("actualcost"),
									rs.getString("milestones"),
									rs.getDate("startdate"),
									rs.getDate("enddate"),
									rs.getString("stakeholder"),
									rs.getString("description"),
									pf,
									customer,
									risk,
									phase
								);
			list.add(project);
		}
		return list;
	}
	
	
	/**
	 * calculate the status of an project
	 */		
	public String calculateStatus(long id) {	
		String status ="all";
		try {
			Project project = getById(id);
			
			boolean isInBudget = false;
			boolean isInTime = false;
			int riskClass = project.getRisks().getId();
			double startDays = project.getStartDate().getTime() / (24 * 60 * 60 * 1000);
			double endDays = project.getEndDate().getTime() / (24 * 60 * 60 * 1000);
			double currentDays = new Date().getTime() / (24 * 60 * 60 * 1000);		
			double totalDays = endDays - startDays + 1;
			double usedDays = currentDays - startDays;
			
			if(project.getProjectPhase().getId() == 6) {
				status = "end";
				return status;
			}
			
			isInBudget = project.getBudget() > project.getActualCost();
			isInTime = totalDays > usedDays;
			
			if(isInBudget && isInTime) {
				status = "green";
			}
			else if(isInBudget && !isInTime || !isInBudget && isInTime) {
				switch(riskClass) {
					case 1:
					case 2:
						status = "yellow";
						break;
					case 3:
					case 4:
					case 5:
						status = "red";
						break;				
				}
			}
			else if(!isInBudget && !isInTime ) {
				status = "red";
			}
			
			return status;
		} catch (Exception e) {
			return status;
		}
	}
	
	/**
	 * calculate earned Value of an project
	 */	
	public int calculateEarnedValue(long id) {
		int value = 0;
		try {
			Project project = getById(id);
			Date today = new Date();	
			
			if(today.after(project.getEndDate())) {
			    return project.getBudget();
			}
			if(today.before(project.getStartDate())) {
				return value;
			}
			

			double startDays = project.getStartDate().getTime() / (24 * 60 * 60 * 1000);
			double endDays = project.getEndDate().getTime() / (24 * 60 * 60 * 1000);
			double currentDays = new Date().getTime() / (24 * 60 * 60 * 1000);	

			double q = Math.abs(currentDays-startDays);
			double d = Math.abs(endDays-startDays);
			double progress = q/d;	
			value = (int) (project.getBudget()*progress);
			
			return value;
			
		} catch (Exception e) {
			return value;
		}
	}

	/**
	 * Helper to convert sql return in employee class
	 * @throws SQLException 
	 *
	 */
	private Collection<Integer> mapEmployees(ResultSet rs) throws SQLException 
	{
		LinkedList<Integer> employees = new LinkedList<Integer>();
		while(rs.next())
		{
			int id = Integer.parseInt(rs.getString("fk_employee"));
			employees.add(id);
		}
		return employees;
	}
}