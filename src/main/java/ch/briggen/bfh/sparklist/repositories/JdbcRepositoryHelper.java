package ch.briggen.bfh.sparklist.repositories;

import java.sql.Connection;
import java.sql.SQLException;

import ch.briggen.sparkbase.JdbcSparkApp;

/**
 * Helper to get the DB connection (added by marcel briggen)
 *
 */
public class JdbcRepositoryHelper {
	/**
	 * Returns the DB connection
	 * @throws SQLException
	 */
	public static Connection getConnection() throws SQLException
	{
		return JdbcSparkApp.getApplication().getJdbcConnection();
	}

}
