package ch.briggen.bfh.sparklist.webhelpers;

import ch.briggen.bfh.sparklist.domain.Customer;
import spark.Request;
/**
 * Helper to create a Customer object from form-data
 *
 */
public class CustomerWebHelper {
	/**
	 * Create a Customer object from form-data
	 * @param request a webrequest containing form data
	 * @return an object containing the passed data
	 */
	public static Customer customerFromWeb(Request request)
	{		
		return new Customer(
				Integer.parseInt(request.queryParams("customer.id")),
				request.queryParams("customer.name"),
				request.queryParams("customer.firstname"),
				request.queryParams("customer.adress"),
				request.queryParams("customer.phone"),
				request.queryParams("customer.email"),
				request.queryParams("customer.company"),
				request.queryParams("customer.state"),
				request.queryParams("customer.zipCode")
				);		
	}
}
