package ch.briggen.bfh.sparklist.webhelpers;

import java.util.Date;

import ch.briggen.bfh.sparklist.DateHelper;
import ch.briggen.bfh.sparklist.domain.Customer;
import ch.briggen.bfh.sparklist.domain.Portfolio;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectPhase;
import ch.briggen.bfh.sparklist.domain.ProjectRisk;
import ch.briggen.bfh.sparklist.repositories.CustomerRepository;
import ch.briggen.bfh.sparklist.repositories.PortfolioRepository;
import ch.briggen.bfh.sparklist.repositories.ProjectPhaseRepository;
import ch.briggen.bfh.sparklist.repositories.ProjectRiskRepository;
import spark.Request;
/**
 * Helper to create a Project object from form-data
 *
 */
public class ProjectWebHelper {
	
	private static CustomerRepository customerRepo = new CustomerRepository();
	private static PortfolioRepository portfolioRepo = new PortfolioRepository();
	private static ProjectRiskRepository riskRepo = new ProjectRiskRepository();
	private static ProjectPhaseRepository phaseRepo = new ProjectPhaseRepository();
	/**
	 * Create a Project object from form-data
	 * @param request a webrequest containing form data
	 * @return an object containing the passed data
	 */
	public static Project projectFromWeb(Request request)
	{
		//convert dates to correct format
		Date startDate = DateHelper.StringToDate(request.queryParams("projectDetail.startDate"));
		Date endDate = DateHelper.StringToDate(request.queryParams("projectDetail.endDate"));
		//load related objects from db
		Customer customer = customerRepo.getById(Long.parseLong(request.queryParams("projectDetail.customer")));
		Portfolio pf = portfolioRepo.getById(Long.parseLong(request.queryParams("projectDetail.portfolio")));
		ProjectRisk risk = riskRepo.getById(Integer.parseInt(request.queryParams("projectDetail.risks")));
		ProjectPhase phase = phaseRepo.getById(Integer.parseInt(request.queryParams("projectDetail.projectPhase")));
		
		Project p = new Project(
				Long.parseLong(request.queryParams("projectDetail.id")),
				Integer.parseInt(request.queryParams("projectDetail.isActive")),
				request.queryParams("projectDetail.name"),
				request.queryParams("projectDetail.owner"),
				Integer.parseInt(request.queryParams("projectDetail.budget")),
				Integer.parseInt(request.queryParams("projectDetail.plannedValue")),
				Integer.parseInt(request.queryParams("projectDetail.actualCost")),
				request.queryParams("projectDetail.milestone"),
				startDate,
				endDate,
				request.queryParams("projectDetail.stakeholder"),
				request.queryParams("projectDetail.description"),
				pf,
				customer,
				risk,
				phase
				);
		return p;
	}
}
