package ch.briggen.bfh.sparklist.webhelpers;

import java.util.Date;

import ch.briggen.bfh.sparklist.DateHelper;
import ch.briggen.bfh.sparklist.domain.Config;
import spark.Request;

/**
 * Helper to create a Config object from form-data
 *
 */
public class ConfigWebHelper {
	
	/**
	 * Create a Config object from form-data
	 * @param request a webrequest containing form data
	 * @return an object containing the passed data
	 */
	public static Config configFromWeb(Request request)
	{
		//convert dates to correct format first
		Date expiryDate = DateHelper.StringToDate(request.queryParams("configDetails.expiryDate"));
		return new Config(
				Integer.parseInt(request.queryParams("configDetails.id")),
				request.queryParams("configDetails.companyName"),
				request.queryParams("configDetails.adress"),
				request.queryParams("configDetails.zip"),
				request.queryParams("configDetails.state"),
				request.queryParams("configDetails.licenceKey"),
				expiryDate
				);		
	}
}
