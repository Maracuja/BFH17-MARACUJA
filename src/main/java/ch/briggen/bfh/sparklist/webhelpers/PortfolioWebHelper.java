package ch.briggen.bfh.sparklist.webhelpers;

import ch.briggen.bfh.sparklist.domain.Portfolio;
import spark.Request;
/**
 * Helper to create a Portfolio object from form-data
 *
 */
public class PortfolioWebHelper {	
	/**
	 * Create a Portfolio object from form-data
	 * @param request a webrequest containing form data
	 * @return an object containing the passed data
	 */
	public static Portfolio portfolioFromWeb(Request request) 
	{
		return new Portfolio(
				Long.parseLong(request.queryParams("portfolio.id")),
				Integer.parseInt(request.queryParams("portfolio.isActive")),
				request.queryParams("portfolio.name"),
				request.queryParams("portfolio.description")
				);		
	}
}
