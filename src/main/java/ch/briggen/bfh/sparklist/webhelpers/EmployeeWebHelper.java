package ch.briggen.bfh.sparklist.webhelpers;

import java.math.BigDecimal;
import ch.briggen.bfh.sparklist.domain.Employee;
import spark.Request;
/**
 * Helper to create a Employee object from form-data
 *
 */
public class EmployeeWebHelper {
	/**
	 * Create a Employee object from form-data
	 * @param request a webrequest containing form data
	 * @return an object containing the passed data
	 */
	public static Employee employeeFromWeb(Request request)
	{
		//convert rate to correct format
		BigDecimal bd =new BigDecimal(request.queryParams("employee.rate"));
		return new Employee(
				Integer.parseInt(request.queryParams("employee.id")),
				request.queryParams("employee.firstname"),
				request.queryParams("employee.lastname"),
				request.queryParams("employee.section"),
				request.queryParams("employee.function"),
				bd
				);		
	}
}
