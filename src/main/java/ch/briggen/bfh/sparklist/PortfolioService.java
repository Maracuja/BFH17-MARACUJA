package ch.briggen.bfh.sparklist;

import ch.briggen.bfh.sparklist.domain.Portfolio;
import ch.briggen.bfh.sparklist.repositories.PortfolioRepository;

/*
 * PortfolioService class serves as an API for portfolio-data and -stats.
 * Has endpoints for chart-data and more
 */
public class PortfolioService {
	// portfolio-obj to get data from
	private Portfolio portfolio;
    PortfolioRepository portfolioRepo = new PortfolioRepository();
	
	//constructor. Initializes a repo and gets the portfolio from the db
	public PortfolioService(long id) {
        portfolio = portfolioRepo.getById(id);
	}

	/*
	 * Returns the budget and actualCost values of all projects in a portfolio
	 */
	public Object getBudgetProgressChart() {
		double[] data = {portfolioRepo.getAllBudgets(portfolio.getId()), portfolioRepo.getAllCosts(portfolio.getId())};
		return data;
	}
	
	/*
	 * Returns the number of projects in a portfolio
	 */
	public Object getProjectChart() {
		return portfolioRepo.countProjects(portfolio.getId());
	}	
}