package ch.briggen.bfh.sparklist;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Helper class with useful functions to convert strings to dates and alike
 */
public class DateHelper {
	//logger instance
	private final static Logger log = LoggerFactory.getLogger(DateHelper.class);
	
	//format to use when converting dates
	private static final String dateFormat = "yyyy-MM-dd";
	
	/*
	 * Converts a passed string to a java Date object
	 */
	public static Date StringToDate(String sDate)
	{
		try {
			DateFormat format = new SimpleDateFormat(dateFormat);
			return format.parse(sDate);
		} catch (Exception e) {
			log.trace("error converting string to date. string: " + sDate + " Exception: " + e.getMessage());
			return null;
		}
	}
	
	/*
	 * Converts a string to a sql Date object
	 */
	public static java.sql.Date StringToSqlDate(String sDate)
	{
		try {
			DateFormat format = new SimpleDateFormat(dateFormat);
			Date d = format.parse(sDate);
			return DateToSqlDate(d);
		} catch (Exception e) {
			log.trace("error converting string to sqlDate. string: " + sDate + " Exception: " + e.getMessage());
			return null;
		}
	}
	
	/*
	 * Converts a java Date to a sql Date object
	 */
	public static java.sql.Date DateToSqlDate(Date d)
	{
		try {
			return new java.sql.Date(d.getTime());
		} catch (Exception e) {
			log.trace("error converting date to sqlDate. date: " + d + " Exception: " + e.getMessage());
			return null;
		}
	}
}
