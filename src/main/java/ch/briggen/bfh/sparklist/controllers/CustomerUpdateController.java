package ch.briggen.bfh.sparklist.controllers;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Customer;
import ch.briggen.bfh.sparklist.repositories.CustomerRepository;
import ch.briggen.bfh.sparklist.webhelpers.CustomerWebHelper;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * The CustomerUpdateController saves changes made to customers
 */
public class CustomerUpdateController implements TemplateViewRoute  {
	private final Logger log = LoggerFactory.getLogger(ProjectUpdateController.class);
	private CustomerRepository customerRepo = new CustomerRepository();
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		try {
			Customer customer = CustomerWebHelper.customerFromWeb(request);		
			log.trace("POST /employee/update mit employee details " + customer);	
			//decide wheter new customer or update existing
			if(customer.getId() == 0)
			{
				customerRepo.insert(customer);
			}
			else {
				customerRepo.update(customer);
			}

			response.redirect("/config");
		} catch (Exception e) {
			//redirect to error 
			HashMap<Object, Object> model = new HashMap<Object, Object>();	
			model.put("errorMessage", e);
			return new ModelAndView(model, "errorTemplate");
		}
		return null;
	}
}
