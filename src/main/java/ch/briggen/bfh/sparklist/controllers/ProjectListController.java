package ch.briggen.bfh.sparklist.controllers;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.repositories.PortfolioRepository;
import ch.briggen.bfh.sparklist.repositories.ProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Loads all projects and returns them as a list
 * Template: projectListTemplate.html
 */
public class ProjectListController implements TemplateViewRoute{
	private final Logger log = LoggerFactory.getLogger(ProjectListController.class);
	private ProjectRepository projectRepo = new ProjectRepository();
	private PortfolioRepository portfolioRepo = new PortfolioRepository();
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		log.trace("return all projects");
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("projectCollection", projectRepo.getAll());
		model.put("portfolioCollection", portfolioRepo.getAll());
		
		return new ModelAndView(model, "projectListTemplate");
	}

}
