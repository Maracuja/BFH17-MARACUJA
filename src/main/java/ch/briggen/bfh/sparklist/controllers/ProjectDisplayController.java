package ch.briggen.bfh.sparklist.controllers;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.repositories.PortfolioRepository;
import ch.briggen.bfh.sparklist.repositories.ProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;
/**
 * Loads data of a project and returns the detail-view of it
 * Paramter:
 * - string: id
 * Template: projectDisplayTemplate.html
 */
public class ProjectDisplayController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjectListController.class);
	private ProjectRepository projectRepo = new ProjectRepository();
	private PortfolioRepository portfolioRepo = new PortfolioRepository();

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
		
		if(null == idString)
		{
			response.redirect("/projects");
		}
		else
		{
			log.trace("GET /project mit id " + idString);
			Long id = Long.parseLong(idString);
			Project p = projectRepo.getById(id);
			model.put("projectDetail", p);
			model.put("projectCollection", projectRepo.getByPortfolio(p.getPortfolio().getId()));
		}

		model.put("portfolioCollection", portfolioRepo.getAll());
		return new ModelAndView(model, "projectDisplayTemplate");
	}

}
