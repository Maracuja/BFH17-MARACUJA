package ch.briggen.bfh.sparklist.controllers;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Portfolio;
import ch.briggen.bfh.sparklist.repositories.PortfolioRepository;
import ch.briggen.bfh.sparklist.webhelpers.PortfolioWebHelper;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Saves changes of a portfolio or creates a new one
 */
public class PortfolioUpdateController implements TemplateViewRoute  {
	private final Logger log = LoggerFactory.getLogger(ProjectUpdateController.class);
	private PortfolioRepository portfolioRepo = new PortfolioRepository();
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		try {
			Portfolio portfolio = PortfolioWebHelper.portfolioFromWeb(request);		
			log.trace("POST /item/update mit itemDetail " + portfolio);		
			
			if(portfolio.getName().isEmpty()){
				//redirect to error 
				HashMap<Object, Object> model = new HashMap<Object, Object>();	
				model.put("errorMessage", "Name darf nicht leer sein.");
				return new ModelAndView(model, "errorTemplate");
			}
			
			long redirectId;
			//Check wheter to create a new portfolio or to update an existing one.
			if(portfolio.getId() == 0){
				redirectId = portfolioRepo.insert(portfolio);
			}else{
				redirectId = portfolioRepo.update(portfolio);
			}
			
			response.redirect("/portfolio?id=" + redirectId);
		} catch (Exception e) {
			//redirect to error 
			HashMap<Object, Object> model = new HashMap<Object, Object>();	
			model.put("errorMessage", e);
			return new ModelAndView(model, "errorTemplate");
		}
		return null;
	}

}
