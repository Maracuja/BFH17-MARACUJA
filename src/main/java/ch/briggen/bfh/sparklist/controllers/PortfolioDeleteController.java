package ch.briggen.bfh.sparklist.controllers;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Portfolio;
import ch.briggen.bfh.sparklist.repositories.PortfolioRepository;
import ch.briggen.bfh.sparklist.repositories.ProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * The PortfolioDeleteController "deletes" portfolios by setting them to inactive
 * Paramter:
 * - string: id
 */
public class PortfolioDeleteController implements TemplateViewRoute{
	private final Logger log = LoggerFactory.getLogger(ProjectDeleteController.class);
	private PortfolioRepository portfolioRepo = new PortfolioRepository();
	private ProjectRepository projectRepo = new ProjectRepository();
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		log.trace("GET /portfolio/delete with idString " + idString);
		
		//check for id, abort if none
		if(null == idString)
		{
			log.trace("GET PortfolioDeleteController /portfolio without idString");	
		} 
		else
		{
			Long id = Long.parseLong(idString);
			//check if portfolio still has active projects
			if(projectRepo.getByPortfolio(id).isEmpty())
			{
				//set portfolio to inactive instead of deleting it to keep data
				log.trace("set portfolio to inactive");
				
				try{
					//get project from db, edit and save it
					Portfolio p = portfolioRepo.getById(id);
					p.setIsActive(0);
					portfolioRepo.update(p);
				}
				catch(Exception e){
					log.error("Error updating portfolio.", e);
					//redirect the user to existing portfolio
					response.redirect("/portfolio?id=" + idString);
				}				
				//redirect the user to start after deletion
				response.redirect("/");
			}
			else{
				log.trace("can't delete portfolio, has active projects");
				response.redirect("/portfolio?id=" + idString);
			}
		}
		
		//redirect to error in case something goes wrong
		HashMap<Object, Object> model = new HashMap<Object, Object>();				
		return new ModelAndView(model, "errorTemplate");
	}

}
