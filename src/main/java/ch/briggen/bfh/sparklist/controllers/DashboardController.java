package ch.briggen.bfh.sparklist.controllers;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.repositories.PortfolioRepository;
import ch.briggen.bfh.sparklist.repositories.ProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * The DashboardController returns the Dashboard of the application
 */
public class DashboardController  implements TemplateViewRoute{
	private final Logger log = LoggerFactory.getLogger(PortfolioDisplayController.class);
	private PortfolioRepository portfolioRepo = new PortfolioRepository();
	private ProjectRepository projectRepo = new ProjectRepository();
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		log.trace("load dashboard");
		HashMap<String, Object> model = new HashMap<String, Object>();

		model.put("portfolioCollection", portfolioRepo.getAll());
		model.put("projectCollection", projectRepo.getAll());
		model.put("menuProjectCollection", projectRepo.getAll());
		
		return new ModelAndView(model, "dashboardTemplate");
	}

}
