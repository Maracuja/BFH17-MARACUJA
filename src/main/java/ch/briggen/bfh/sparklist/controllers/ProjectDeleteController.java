package ch.briggen.bfh.sparklist.controllers;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.repositories.ProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * The DeleteController "deletes" Projects by setting them to inactive
 * Paramter:
 * - string: id
 */
public class ProjectDeleteController implements TemplateViewRoute {
	private final Logger log = LoggerFactory.getLogger(ProjectDeleteController.class);
	private ProjectRepository projectRepo = new ProjectRepository();

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		log.trace("POST /project/delete with idString " + idString);
		
		//check for id, abort if none
		if(null == idString)
		{
			log.trace("POST ProjectDeleteController /project without idString");	
		} 
		else
		{
			//set project to inactive instead of deleting it to keep data
			log.trace("set project to inactive");
			Long id = Long.parseLong(idString);
			
			try
			{
				//get project from db, edit and save it
				Project p = projectRepo.getById(id);
				p.setIsActive(0);
				projectRepo.update(p);
				
				//redirect the user
				response.redirect("/portfolio?id=" + p.getPortfolio().getId());
			}
			catch(Exception e)
			{
				log.error("Error deleting a project.", e);
			}				
		}
		//redirect to error in case something goes wrong
		HashMap<Object, Object> model = new HashMap<Object, Object>();				
		return new ModelAndView(model, "errorTemplate");
	}
}
