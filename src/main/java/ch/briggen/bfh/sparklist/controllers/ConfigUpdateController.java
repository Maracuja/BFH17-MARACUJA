package ch.briggen.bfh.sparklist.controllers;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Config;
import ch.briggen.bfh.sparklist.repositories.ConfigRepository;
import ch.briggen.bfh.sparklist.webhelpers.ConfigWebHelper;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * The ConfigUpdateController saves configuration-data entered into the config-page of the application
 */
public class ConfigUpdateController implements TemplateViewRoute  {
	private final Logger log = LoggerFactory.getLogger(ProjectUpdateController.class);
	private ConfigRepository configRepo = new ConfigRepository();
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		try {
			Config config = ConfigWebHelper.configFromWeb(request);		
			log.trace("POST /config/update mit config details " + config);	
			//save it
			configRepo.update(config);
			
			response.redirect("/config");
		} catch (Exception e) {
			//redirect to error 
			HashMap<Object, Object> model = new HashMap<Object, Object>();	
			model.put("errorMessage", e);
			return new ModelAndView(model, "errorTemplate");
		}
		return null;
	}
}
