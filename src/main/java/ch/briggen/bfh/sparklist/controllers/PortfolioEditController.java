package ch.briggen.bfh.sparklist.controllers;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Portfolio;
import ch.briggen.bfh.sparklist.repositories.PortfolioRepository;
import ch.briggen.bfh.sparklist.repositories.ProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Loads data of a portfolio and returns the view to edit it
 * Parameter:
 * - string: id
 * Template: portfolioEditTemplate.html
 */
public class PortfolioEditController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(PortfolioListController.class);
	private PortfolioRepository portfolioRepo = new PortfolioRepository();
	private ProjectRepository projectRepo = new ProjectRepository();
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
		
		Portfolio portfolio;
		//if no id is passed a new empty portfolio will be created and used in the edit-template
		if(null == idString)
		{
			log.trace("Id is null - create new portfolio");
			portfolio = new Portfolio();
			model.put("projectCollection", projectRepo.getAll());
		}
		else
		{
			//update existing portfolio
			log.trace("Portfolio ID is:" + idString + ". Return edit");
			Long id = Long.parseLong(idString);
			portfolio = portfolioRepo.getById(id);
			model.put("projectCollection", projectRepo.getByPortfolio(id));
		}
		model.put("portfolio", portfolio);
		model.put("postAction", "/portfolio/update"); //action for when the form is submitted, used in the editTemplate
		model.put("portfolioCollection", portfolioRepo.getAll());
		
		return new ModelAndView(model, "portfolioEditTemplate");
	}

}
