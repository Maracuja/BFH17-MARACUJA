package ch.briggen.bfh.sparklist.controllers;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Customer;
import ch.briggen.bfh.sparklist.domain.Employee;
import ch.briggen.bfh.sparklist.repositories.ConfigRepository;
import ch.briggen.bfh.sparklist.repositories.CustomerRepository;
import ch.briggen.bfh.sparklist.repositories.EmployeeRepository;
import ch.briggen.bfh.sparklist.repositories.PortfolioRepository;
import ch.briggen.bfh.sparklist.repositories.ProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Returns the configuration-page of the application
 * Template: configTemplate.html
 */
public class ConfigController  implements TemplateViewRoute{
	//logger- and repo-instances
	private final Logger log = LoggerFactory.getLogger(PortfolioDisplayController.class);
	private PortfolioRepository portfolioRepo = new PortfolioRepository();
	private ProjectRepository projectRepo = new ProjectRepository();
	private ConfigRepository configRepo = new ConfigRepository();
	private EmployeeRepository employeeRepo = new EmployeeRepository();
	private CustomerRepository customerRepo = new CustomerRepository();
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		log.trace("return all portfolios");
		
		//fill model and return it
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("postAction", "/config/update"); //action for when the form is submitted, used in the editTemplate
		model.put("portfolioCollection", portfolioRepo.getAll());
		model.put("projectCollection", projectRepo.getAll());
		model.put("menuProjectCollection", projectRepo.getAll());
		model.put("employeeCollection", employeeRepo.getAll());
		model.put("postActionEmployee", "/employee/update");
		model.put("customerCollection", customerRepo.getAll());
		model.put("postActionCustomer", "/customer/update");
		model.put("configDetails", configRepo.getById(1));
		model.put("emptyEmployee", new Employee());
		model.put("emptyCustomer", new Customer());
		
		return new ModelAndView(model, "configTemplate");
	}

}
