package ch.briggen.bfh.sparklist.controllers;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Employee;
import ch.briggen.bfh.sparklist.repositories.EmployeeRepository;
import ch.briggen.bfh.sparklist.webhelpers.EmployeeWebHelper;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;
/**
 * Saves changes to employees made on configuration-page
 *
 */
public class EmployeeUpdateController implements TemplateViewRoute  {
	private final Logger log = LoggerFactory.getLogger(ProjectUpdateController.class);
	private EmployeeRepository employeeRepo = new EmployeeRepository();
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		try {
			Employee employee = EmployeeWebHelper.employeeFromWeb(request);		
			log.trace("POST /employee/update mit employee details " + employee);	
			//check if new employee or update an existing one
			if(employee.getId() == 0) {
				employeeRepo.insert(employee);			
			} else 
			{
				employeeRepo.update(employee);
			}

			response.redirect("/config");
		} catch (Exception e) {
			//redirect to error 
			HashMap<Object, Object> model = new HashMap<Object, Object>();	
			model.put("errorMessage", e);
			return new ModelAndView(model, "errorTemplate");
		}
		return null;
	}
}
