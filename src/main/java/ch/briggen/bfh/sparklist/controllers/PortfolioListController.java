package ch.briggen.bfh.sparklist.controllers;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.repositories.PortfolioRepository;
import ch.briggen.bfh.sparklist.repositories.ProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Loads all portfolios from the db and returns a list of them
 * Template: portfolioListTemplate.html
 */
public class PortfolioListController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjectListController.class);
	private PortfolioRepository portfolioRepo = new PortfolioRepository();
	private ProjectRepository projectRepo = new ProjectRepository();
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		log.trace("return all portfolios");
		HashMap<String, Object> model = new HashMap<String, Object>();
		
		model.put("portfolioCollection", portfolioRepo.getAll());
		model.put("projectCollection", projectRepo.getAll());
		
		return new ModelAndView(model, "portfolioListTemplate");
	}

}
