package ch.briggen.bfh.sparklist.controllers;

import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Portfolio;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.repositories.PortfolioRepository;
import ch.briggen.bfh.sparklist.repositories.ProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;
/**
 * Loads portfolio-data and returns a detail-view of a portfolio
 * Paramter: 
 * - string: id
 * Template: portfolioDisplayTemplate.html
 */
public class PortfolioDisplayController implements TemplateViewRoute{

	int riskCalculation = 0;
	int stateCalculation = 0;
	private final Logger log = LoggerFactory.getLogger(PortfolioDisplayController.class);
	private PortfolioRepository portfolioRepo = new PortfolioRepository();
	private ProjectRepository projectRepo = new ProjectRepository();
	
	/**
	 *  Liefert daten des portolfios zur�ck, wird mit /portfolio aufgerufen 
	 *  @return Model 
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		log.trace("load a detail-view of a portfolio");
		HashMap<String, Object> model = new HashMap<String, Object>();
		Long id = Long.parseLong(request.queryParams("id"));
		int countProjetMembers = 0;
		int countEarnedValue = 0;
		Portfolio portfolio = portfolioRepo.getById(id);		
		
		for(Project project : projectRepo.getByPortfolio(id)) {
			countProjetMembers += project.getProjectEmployeeIDs().size();
			countEarnedValue += project.getEarnedValue();
		}
		model.put("portfolioState", portfolioRepo.calculatePortfolioStatus(id));
		model.put("portfolioRisk", portfolioRepo.calculatePortfolioRisks(id));
		model.put("earnedValue", countEarnedValue);
		model.put("members", countProjetMembers);
		model.put("portfolio", portfolio);
		model.put("projectCollection", projectRepo.getByPortfolio(id));
		model.put("portfolioCollection", portfolioRepo.getAll());
		
		return new ModelAndView(model, "portfolioDisplayTemplate");
	}
}
