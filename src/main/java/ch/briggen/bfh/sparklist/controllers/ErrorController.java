package ch.briggen.bfh.sparklist.controllers;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * The ErrorController returns the generic error page
 */
public class ErrorController  implements TemplateViewRoute{
	private final Logger log = LoggerFactory.getLogger(ErrorController.class);
		
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		log.trace("load error page");
		HashMap<Object, Object> model = new HashMap<Object, Object>();				
		return new ModelAndView(model, "errorTemplate");
	}

}
