package ch.briggen.bfh.sparklist.controllers;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.repositories.ProjectRepository;
import ch.briggen.bfh.sparklist.webhelpers.ProjectWebHelper;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Saves changes to an existing project or creates a new one
 * 
 */
public class ProjectUpdateController implements TemplateViewRoute  {
	private final Logger log = LoggerFactory.getLogger(ProjectUpdateController.class);
	private ProjectRepository projectRepo = new ProjectRepository();
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		try {
			Project project = ProjectWebHelper.projectFromWeb(request);		
			
			long redirectId;
			//check wheter to create a new project or to save an existing
			if(project.getId() == 0){
				log.trace("insert new project");
				redirectId = projectRepo.insert(project);
			}else{
				log.trace("POST /item/update mit itemDetail " + project);
				redirectId = projectRepo.update(project);
				projectRepo.DeleteProjectEmployee(redirectId);
			}
			
			if(request.queryParamsValues("projectDetail.employees") != null) {
				for (String employeeId : request.queryParamsValues("projectDetail.employees")) {
					   int id = Integer.parseInt(employeeId);
					   projectRepo.insertProjectEmployee(redirectId, id);
				}
			}
			
			response.redirect("/project?id=" + redirectId);
		} catch (Exception e) {
			//redirect to error 
			HashMap<Object, Object> model = new HashMap<Object, Object>();	
			model.put("errorMessage", e);
			return new ModelAndView(model, "errorTemplate");
		}
		return null;
	}

}
