package ch.briggen.bfh.sparklist.controllers;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.repositories.CustomerRepository;
import ch.briggen.bfh.sparklist.repositories.PortfolioRepository;
import ch.briggen.bfh.sparklist.repositories.ProjectPhaseRepository;
import ch.briggen.bfh.sparklist.repositories.ProjectRepository;
import ch.briggen.bfh.sparklist.repositories.ProjectRiskRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Handles calls to project/edit
 * Parameter:
 * - string: id
 * Template: projectEdit.html
 * 
 * Gets a project from the db and passes it to the template
 * If no querystring-id is passed, a new empty project-object is created and passed to the template
 */
public class ProjectEditController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjectListController.class);
	private ProjectRepository projectRepo = new ProjectRepository();
	private PortfolioRepository portfolioRepo = new PortfolioRepository();
	private CustomerRepository customerRepo = new CustomerRepository();
	private ProjectRiskRepository riskRepo = new ProjectRiskRepository();
	private ProjectPhaseRepository phaseRepo = new ProjectPhaseRepository();
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
		
		Project p;
		//check if querystring was passed
		if(null == idString)
		{
			//create a new emtpy project-obj
			log.trace("GET /project with id " + idString);
			p = new Project();
			//check for querystring "portfolioId" and use to assign correct portfolio 
			String idPortfolioString = request.queryParams("portfolioId");
			if(null != idPortfolioString)
			{
				p.setPortfolio(portfolioRepo.getById(Long.parseLong(idPortfolioString)));
			}
		}
		else
		{
			//get the project from the db according to passed querystring-id
			log.trace("GET /project mit id " + idString);
			Long id = Long.parseLong(idString);
			p = projectRepo.getById(id);
		}
		//fill data to model
		model.put("projectDetail", p);
		model.put("postAction", "/project/update"); //action for when the form is submitted, used in the editTemplate
		model.put("portfolioCollection", portfolioRepo.getAll()); //for dropdown
		model.put("customerCollection", customerRepo.getAll()); //for dropdown
		model.put("riskCollection", riskRepo.getAllRisks()); //for dropdown
		model.put("projectPhaseCollection", phaseRepo.getAllPhases());//for dropdown
		model.put("projectCollection", projectRepo.getByPortfolio(p.getPortfolio().getId()));
		
		return new ModelAndView(model, "projectEditTemplate");
	}

}
