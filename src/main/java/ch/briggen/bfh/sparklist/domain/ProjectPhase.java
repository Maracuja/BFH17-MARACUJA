package ch.briggen.bfh.sparklist.domain;

/**
 * Project phase element
 */
public class ProjectPhase {
	//props
	private int id;
	private String phase;
	
	/**
	 * Constructor
	 */
	public ProjectPhase() {
		
	}

	/**
	 * Constructor
	 * @param id
	 * @param phase
	 */
	public ProjectPhase(int id, String phase) {
		super();
		this.id = id;
		this.phase = phase;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the risk
	 */
	public String getPhase() {
		return phase;
	}
	/**
	 * @param status the status to set
	 */
	public void setPhase(String phase) {
		this.phase = phase;
	}

	/* @Override
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProjectPhase [id=" + id + ", phase=" + phase + "]";
	}
}
