package ch.briggen.bfh.sparklist.domain;

import ch.briggen.bfh.sparklist.repositories.PortfolioRepository;

/**
 * Portfolio class
 */
public class Portfolio {
	//props
	private long id;
	private int isActive;
	private String name;
	private String description;
	/**
	 * Defaultkonstruktor f�r die Verwendung in einem Controller
	 */
	public Portfolio()
	{
	}

	/**
	 * Contructor
	 * @param id 
	 * @param name
	 * @param description
	 * @param fk_config
	 */
	public Portfolio(long id, int isActive, String name, String description) {
		super();
		this.id = id;
		this.isActive = isActive;
		this.name = name;
		this.description = description;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the isActive-status
	 */
	public int getIsActive(){
		return isActive;
	}
	/**
	 * @param the status to set
	 */
	public void setIsActive(int i) {
		this.isActive = i;
	}
	/**
	 * @param the amout of projects
	 */
	public int getProjectNumber() {
		PortfolioRepository repo = new PortfolioRepository();
		return repo.countProjects(this.id);
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		PortfolioRepository repo = new PortfolioRepository();
		return repo.calculatePortfolioStatus(this.id);
	}
	
	/**
	 * @Override
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "portfolio [id=" + id + ", isActive=" + isActive + ", name=" + name + ", description=" + description + ", status=" + getStatus()
				+ "]";
	}
}

	
	