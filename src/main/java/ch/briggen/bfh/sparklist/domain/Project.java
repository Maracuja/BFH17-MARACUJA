package ch.briggen.bfh.sparklist.domain;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;

import ch.briggen.bfh.sparklist.repositories.EmployeeRepository;
import ch.briggen.bfh.sparklist.repositories.ProjectRepository;
/**
 * Single project element
 */
public class Project {
	//props
	private long id;
	private int isActive;
	private String name;
	private String owner;
	private ProjectPhase projectPhase;
	private int budget;
	private int plannedValue;
	private int actualCost;
	private String milestone;
	private Date startDate;
	private Date endDate;
	private String stakeholder;
	private ProjectRisk risks;
	private String description;
	private Portfolio portfolio;
	private Customer customer;
	private String url;
	private ProjectRepository projectRepo = new ProjectRepository();
	
	/**
	 * Constructor
	 */
	public Project()
	{		
	}
	
	/**
	 *  Constructor
	 * @param id
	 * @param isActive
	 * @param name
	 * @param owner
	 * @param i
	 * @param budget
	 * @param plannedValue
	 * @param actualCost
	 * @param string
	 * @param date
	 * @param startDate
	 * @param string2
	 * @param stakeholder
	 * @param risks
	 * @param description
	 * @param portfolio
	 * @param customer
	 * @param phase
	 */
	public Project(long id, int isActive, String name, String owner, int budget, int plannedValue,
			int actualCost, String milestone, Date startDate, Date endDate, String stakeholder, String description, Portfolio portfolio, Customer customer, ProjectRisk risk, ProjectPhase projectPhase) {
		super();
		this.id = id;
		this.isActive = isActive;
		this.name = name;
		this.owner = owner;
		this.projectPhase = projectPhase;
		this.budget = budget;
		this.plannedValue = plannedValue;
		this.actualCost = actualCost;
		this.milestone = milestone;
		this.startDate = startDate;
		this.endDate = endDate;
		this.stakeholder = stakeholder;
		this.description = description;
		this.portfolio = portfolio;
		this.customer = customer;
		this.risks = risk;
		
		//dynamic values
		this.url = "/project?id=" + id;
	}
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the isActive
	 */
	public int getIsActive() {
		return isActive;
	}
	/**
	 * @param the isActive to set
	 */
	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the owner
	 */
	public String getOwner() {
		return owner;
	}
	/**
	 * @param owner the owner to set
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}
	/**
	 * @return the state
	 */
	public String getStatus() {
		return projectRepo.calculateStatus(id);
	}
	/**
	 * @return the projectPhase
	 */
	public ProjectPhase getProjectPhase() {
		return projectPhase;
	}
	/**
	 * @param projectPhase the projectPhase to set
	 */
	public void setProjectPhase(ProjectPhase projectPhase) {
		this.projectPhase = projectPhase;
	}
	/**
	 * @return the budget
	 */
	public int getBudget() {
		return budget;
	}
	/**
	 * @param budget the budget to set
	 */
	public void setBudget(int budget) {
		this.budget = budget;
	}
	/**
	 * @return the plannedValue
	 */
	public int getPlannedValue() {
		return plannedValue;
	}
	/**
	 * @param plannedValue the plannedValue to set
	 */
	public void setPlannedValue(int plannedValue) {
		this.plannedValue = plannedValue;
	}
	/**
	 * @return the actualCost
	 */
	public int getActualCost() {
		return actualCost;
	}
	/**
	 * @param actualCost the actualCost to set
	 */
	public void setActualCost(int actualCost) {
		this.actualCost = actualCost;
	}
	/**
	 * @return the earnedValue
	 */
	public int getEarnedValue() {
		return projectRepo.calculateEarnedValue(id);
	}
	
	/**
	 * @return the CPI
	 */
	public String getCPI() {
		return String.format("%.4g%n", (double)projectRepo.calculateEarnedValue(id)/(double)actualCost);
	}
	
	/**
	 * @return the SPI
	 */
	public String getSPI() {
		return String.format("%.4g%n", (double)projectRepo.calculateEarnedValue(id)/(double)budget);
	}

	/**
	 * @return the milestone
	 */
	public String getMilestone() {
		return milestone;
	}
	/**
	 * @param milestone the milestone to set
	 */
	public void setMilestone(String milestone) {
		this.milestone = milestone;
	}
	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	/**
	 * @return the stakeholder
	 */
	public String getStakeholder() {
		return stakeholder;
	}
	/**
	 * @param stakeholder the stakeholder to set
	 */
	public void setStakeholder(String stakeholder) {
		this.stakeholder = stakeholder;
	}
	/**
	 * @return the risks
	 */
	public ProjectRisk getRisks() {
		return risks;
	}
	/**
	 * @param risks the risks to set
	 */
	public void setRisks(ProjectRisk risks) {
		this.risks = risks;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the portfolio
	 */
	public Portfolio getPortfolio() {
		return portfolio;
	}
	/**
	 * @param portfolio the portfolio to set
	 */
	public void setPortfolio(Portfolio portfolio) {
		this.portfolio = portfolio;
	}
	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}
	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the employees
	 */
	public Collection<Employee> getEmployees() {
		EmployeeRepository employeeRepo = new EmployeeRepository();
		return employeeRepo.getAll();
	}
	
	/**
	 * @return the project employees
	 */
	public String getProjectEmployeeNames() {
		Collection<Employee> employees = getProjectEmployees();
		String employeeDisplayName = "";
		boolean isFirst = true;
		
		for (Employee employee : employees)
		{
			if(isFirst) {
				employeeDisplayName = employee.getDisplayName(); 
			} else {
				employeeDisplayName = employeeDisplayName + "; " + employee.getDisplayName();
			}

			isFirst = false;
		}
		
		if(employeeDisplayName.isEmpty()) {
			employeeDisplayName = "Keine Teammitglieder definiert";
		}
		
		return employeeDisplayName;		
	}
	
	/**
	 * @return the project employees
	 */
	public LinkedList<Long> getProjectEmployeeIDs() {
		Collection<Employee> employees = getProjectEmployees();
		LinkedList<Long> employeeId = new LinkedList<Long>();
		
		for (Employee employee : employees)
		{
			employeeId.add(employee.getId());
		}
		
		return employeeId;		
	}
	
	/**
	 * @return the project employees
	 */
	private Collection<Employee> getProjectEmployees() {
		EmployeeRepository employeeRepo = new EmployeeRepository();
		LinkedList<Employee> employees = new LinkedList<Employee>();
		for (int employeeId : projectRepo.getEmployeesByProject(this.id))
		{
			employees.add(employeeRepo.getById(employeeId));
		}
		
		return employees;		
	}

	/**
	 * Override toString() method
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Project [id=" + id + ", isActive=" + isActive + ", name=" + name + ", owner=" + owner + ", status=" + getStatus() + ", projectPhase="
				+ projectPhase + ", budget=" + budget + ", plannedValue=" + plannedValue + ", actualCost=" + actualCost
				+ ", earnedValue=" + getEarnedValue() + ", milestone=" + milestone + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", stakeholder=" + stakeholder + ", risks="
				+ risks + ", description=" + description + "]";
	}
}