package ch.briggen.bfh.sparklist.domain;
import java.math.BigDecimal;
/**
 * Single employee element 
 * @author Tobias Luethi
 *
 */
public class Employee {
	//props
	private int id;
	private String firstname;
	private String lastname;
	private String section;
	private String function;
	private java.math.BigDecimal rate;
	
	/**
	 * Constructor
	 */
	public Employee() 
	{
		
	}
	
	/**
	 * Constructor
	 * @param id
	 * @param firstname
	 * @param lastname
	 * @param section
	 * @param function
	 * @param rate
	 */
	public Employee(int id, String firstname, String lastname, String section, String function, BigDecimal rate) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.section = section;
		this.function = function;
		this.rate = rate;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the display name
	 */
	public String getDisplayName() {
		return firstname + " " + lastname;
	}
	/**
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}
	/**
	 * @param firstname the firstname to set
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}
	/**
	 * @param lastname the lastname to set
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	/**
	 * @return the section
	 */
	public String getSection() {
		return section;
	}
	/**
	 * @param section the section to set
	 */
	public void setSection(String section) {
		this.section = section;
	}
	/**
	 * @return the function
	 */
	public String getFunction() {
		return function;
	}
	/**
	 * @param function the function to set
	 */
	public void setFunction(String function) {
		this.function = function;
	}
	/**
	 * @return the rate
	 */
	public BigDecimal getRate() {
		return rate;
	}
	/**
	 * @param rate the rate to set
	 */
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", section=" + section
				+ ", function=" + function + ", rate=" + rate + "]";
	}	
}