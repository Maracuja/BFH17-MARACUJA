package ch.briggen.bfh.sparklist.domain;

/**
 * ProjectRisk element
 *
 */
public class ProjectRisk {
	//props
	private int id;
	private String risk;
	
	/**
	 * Constructor
	 */
	public ProjectRisk() {
		
	}

	/**
	 * Constructor
	 * @param id
	 * @param risk
	 */
	public ProjectRisk(int id, String risk) {
		super();
		this.id = id;
		this.risk = risk;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the risk
	 */
	public String getRisks() {
		return risk;
	}
	/**
	 * @param status the status to set
	 */
	public void setRisks(String risk) {
		this.risk = risk;
	}

	/* @Override
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProjectRisk [id=" + id + ", risk=" + risk + "]";
	}
}
