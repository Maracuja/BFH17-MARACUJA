package ch.briggen.bfh.sparklist;

import com.google.gson.Gson;
import spark.ResponseTransformer;

/*
 * Utility class to convert the responses for AJAX-calls into json-formated data
 */
public class JsonUtil {
	public static String toJson(Object object) {
		return  new Gson().toJson(object);
	}
	public static ResponseTransformer json() {
		return JsonUtil::toJson;
	}
}