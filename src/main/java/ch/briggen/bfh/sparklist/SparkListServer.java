package ch.briggen.bfh.sparklist;

import static spark.Spark.get;
import static spark.Spark.post;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.sparkbase.H2SparkApp;
import ch.briggen.bfh.sparklist.controllers.ConfigController;
import ch.briggen.bfh.sparklist.controllers.ConfigUpdateController;
import ch.briggen.bfh.sparklist.controllers.CustomerUpdateController;
import ch.briggen.bfh.sparklist.controllers.DashboardController;
import ch.briggen.bfh.sparklist.controllers.EmployeeUpdateController;
import ch.briggen.bfh.sparklist.controllers.ErrorController;
import ch.briggen.bfh.sparklist.controllers.PortfolioDeleteController;
import ch.briggen.bfh.sparklist.controllers.PortfolioDisplayController;
import ch.briggen.bfh.sparklist.controllers.PortfolioEditController;
import ch.briggen.bfh.sparklist.controllers.PortfolioListController;
import ch.briggen.bfh.sparklist.controllers.PortfolioUpdateController;
import ch.briggen.bfh.sparklist.controllers.ProjectDeleteController;
import ch.briggen.bfh.sparklist.controllers.ProjectDisplayController;
import ch.briggen.bfh.sparklist.controllers.ProjectEditController;
import ch.briggen.bfh.sparklist.controllers.ProjectListController;
import ch.briggen.bfh.sparklist.controllers.ProjectUpdateController;
import ch.briggen.bfh.sparklist.repositories.ProjectRepository;
import ch.briggen.bfh.sparklist.repositories.RepositoryException;
import spark.template.thymeleaf.ThymeleafTemplateEngine;

/*
 * The SparkListServer class. Contains the main-function. Starts the application and sets up the webserver with db-connection. 
 * Defines routes to use by the application
 */
public class SparkListServer extends H2SparkApp {

    final static Logger log = LoggerFactory.getLogger(SparkListServer.class);

    public static void main(String[] args) {
    	//startup application
    	SparkListServer server = new SparkListServer();
    	server.configure();
    	server.run();
    }

    @Override
	protected void doConfigureHttpHandlers() {
        // main routes
        get("/", new DashboardController(), new ThymeleafTemplateEngine());
        get("search", (req, res) -> {
        	//return all projects
	        ProjectRepository service = new ProjectRepository();
	        return service.getAll();  	        	        
        }, JsonUtil.json());
        get("/error", new ErrorController(), new ThymeleafTemplateEngine());
        
        // configuration routes
        get("/config", new ConfigController(), new ThymeleafTemplateEngine());
        post("/config/update", new ConfigUpdateController(), new ThymeleafTemplateEngine());
        post("/employee/update", new EmployeeUpdateController(), new ThymeleafTemplateEngine());
        post("/customer/update", new CustomerUpdateController(), new ThymeleafTemplateEngine());
        
        //Portfolios (new and edit is the same controller)
        get("/portfolios", new PortfolioListController(), new ThymeleafTemplateEngine());
        get("/portfolio", new PortfolioDisplayController(), new ThymeleafTemplateEngine());
        get("/portfolio/edit", new PortfolioEditController(), new ThymeleafTemplateEngine());
        post("/portfolio/update", new PortfolioUpdateController(), new ThymeleafTemplateEngine());
        get("/portfolio/delete", new PortfolioDeleteController(), new ThymeleafTemplateEngine());
        
        //Projects (new and edit is the same controller)
        get("/projects", new ProjectListController(), new ThymeleafTemplateEngine());        
        get("/project", new ProjectDisplayController(), new ThymeleafTemplateEngine());        
        get("/project/edit", new ProjectEditController(), new ThymeleafTemplateEngine());
        post("/project/update", new ProjectUpdateController(), new ThymeleafTemplateEngine());
        get("/project/delete", new ProjectDeleteController(), new ThymeleafTemplateEngine());        
        
        //API  to get data to the frontend via AJAX 
        get("/api/:element/:type/:id", (req, res) -> {
	        long id = Long.parseLong(req.params(":id"));
	        String type = req.params(":type");
	        String element = req.params(":element");
	        
	        if(element.contains("project"))
	        {
	        	//calls for data about a project
		        ProjectService projectService = new ProjectService(id);
		        if (projectService != null && !type.isEmpty()) {
		        	switch(type){
		        	case "time":
			        	return projectService.getTimeProgressChart();
		        	case "budget":
			        	return projectService.getBudgetProgressChart();
		        	case "customer":
		        		return projectService.getCustomers();
		        	case "employee":
		        		return projectService.getEmployee();
		        	default:
		        		break;
		        	}
		        }	        	
	        }
	        else if(element.contains("portfolio")) 
	        {
	        	//calls for data about a portfolio
		        PortfolioService portfolioService = new PortfolioService(id);
		        if (portfolioService != null && !type.isEmpty()) {
		        	switch(type){
		        	case "budget":
			        	return portfolioService.getBudgetProgressChart();
		        	case "projects":
			        	return portfolioService.getProjectChart();
		        	default:
		        		break;
		        	}
		        }	        	
	        }
	     
	        
	        res.status(400);
	        return new RepositoryException("No project with id " + id + " found");
        }, JsonUtil.json());
	}
}

