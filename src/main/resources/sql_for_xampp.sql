-- -----------------------------------------------------
-- Schema ppm
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS ppm ;
DROP ALL OBJECTS;

-- -----------------------------------------------------
-- Table Customer
-- -----------------------------------------------------
DROP TABLE IF EXISTS Customer ;

CREATE TABLE IF NOT EXISTS Customer (
  pk_idCustomer INT NOT NULL AUTO_INCREMENT,
  Name VARCHAR(60) NULL,
  Firstname VARCHAR(60) NULL,
  Adress VARCHAR(80) NULL,
  Phone VARCHAR(45) NULL,
  Email VARCHAR(45) NULL,
  Company VARCHAR(45) NULL,
  State VARCHAR(45) NULL,
  ZIPCode VARCHAR(45) NULL,
  PRIMARY KEY (pk_idCustomer))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Configuration
-- -----------------------------------------------------
DROP TABLE IF EXISTS Configuration ;

CREATE TABLE IF NOT EXISTS Configuration (
  pk_idConfiguration INT NOT NULL AUTO_INCREMENT,
  Companyname VARCHAR(45) NULL,
  Adress VARCHAR(45) NULL,
  ZIPCode VARCHAR(45) NULL,
  State VARCHAR(45) NULL,
  Licencekey VARCHAR(45) NULL,
  ExpiryDate VARCHAR(45) NULL,
  PRIMARY KEY (pk_idConfiguration))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table ProjectStatus
-- -----------------------------------------------------
DROP TABLE IF EXISTS ProjectStatus ;

CREATE TABLE IF NOT EXISTS ProjectStatus (
   pk_idStatus INT NOT NULL AUTO_INCREMENT,
   Status VARCHAR(20) NULL,
   PRIMARY KEY (pk_idStatus))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table Portfolio
-- -----------------------------------------------------
DROP TABLE IF EXISTS Portfolio ;

CREATE TABLE IF NOT EXISTS Portfolio (
  pk_idPortfolio INT NOT NULL AUTO_INCREMENT,
  IsActive INT NULL,
  Name VARCHAR(80) NULL,
  Description VARCHAR(45) NULL,
  Status VARCHAR(45) NULL,
  PRIMARY KEY (pk_idPortfolio)
  )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table Riskclass
-- -----------------------------------------------------
DROP TABLE IF EXISTS Riskclass ;

CREATE TABLE IF NOT EXISTS Riskclass (
  pk_idRisk INT NOT NULL AUTO_INCREMENT,
  Class INT NULL,
  Text VARCHAR(255) NULL,
  PRIMARY KEY (pk_idRisk))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table Project
-- -----------------------------------------------------
DROP TABLE IF EXISTS Project ;

CREATE TABLE IF NOT EXISTS Project (
  pk_idProject INT NOT NULL AUTO_INCREMENT,
  IsActive INT NULL,
  Name VARCHAR(45) NULL,
  Owner VARCHAR(45) NULL,
  Projectphase VARCHAR(45) NOT NULL,
  Budget INT NULL,
  PlannedValue INT NULL,
  ActualCost INT NULL,
  EarnedValue INT NULL,
  Milestones VARCHAR(45) NULL,
  Startdate TIMESTAMP NULL,
  Enddate TIMESTAMP NULL,
  Stakeholder VARCHAR(60) NULL,
  Description VARCHAR(255) NULL,
  fk_Customer INT NULL,
  fk_Portfolio INT NULL,
  fk_ProjectStatus INT NULL,
  fk_RiskClass INT NULL,
  PRIMARY KEY (pk_idProject),
  CONSTRAINT fk_customer
    FOREIGN KEY (fk_Customer)
    REFERENCES Customer (pk_idCustomer),
  CONSTRAINT fk_Portfolio
    FOREIGN KEY (fk_Portfolio)
    REFERENCES Portfolio (pk_idPortfolio),
  CONSTRAINT fk_projectStatus
    FOREIGN KEY (fk_ProjectStatus)
    REFERENCES ProjectStatus (pk_idStatus),
  CONSTRAINT fk_RiskClass
    FOREIGN KEY (fk_RiskClass)
    REFERENCES Riskclass (pk_idRisk)
    )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table Employee
-- -----------------------------------------------------
DROP TABLE IF EXISTS Employee ;

CREATE TABLE IF NOT EXISTS Employee (
  pk_idEmployee INT NOT NULL AUTO_INCREMENT,
  Firstname VARCHAR(45) NULL,
  Lastname VARCHAR(45) NULL,
  Section VARCHAR(45) NULL,
  Function VARCHAR(45) NULL,
  Rate DECIMAL(5,2) NULL,
  PRIMARY KEY (pk_idEmployee))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table Project_employee
-- -----------------------------------------------------
DROP TABLE IF EXISTS Project_employee ;

CREATE TABLE IF NOT EXISTS Project_employee (
  pk_idProjectEmployee INT NOT NULL AUTO_INCREMENT,
  fk_Employee INT NULL,
  fk_Project INT NULL,
  PRIMARY KEY (pk_idProjectEmployee),
  CONSTRAINT fk_Employee
    FOREIGN KEY (fk_Employee)
    REFERENCES Employee (pk_idEmployee),
  CONSTRAINT fk_Project
    FOREIGN KEY (fk_Project)
    REFERENCES Project (pk_idProject)
    )
ENGINE = InnoDB;

CREATE INDEX fk_Employee_idx ON Project_employee (fk_Employee ASC);

CREATE INDEX fk_Project_idx ON Project_employee (fk_Project ASC);

CREATE INDEX fk_customer_idx ON Project (fk_Customer ASC);

CREATE INDEX fk_Portfolio_idx ON Project (fk_Portfolio ASC);

CREATE INDEX fk_projectStatus_idx ON Project (fk_ProjectStatus ASC);

CREATE INDEX fk_riskClass_idx ON Project (fk_RiskClass ASC);

-- Testdaten --
-- Configuration-Tabelle bef�llen mit Testdaten --
INSERT INTO Configuration VALUES (1, 'RUAG', 'Together-Strasse', '3000', 'Bern', '1234-5678-0936', '13.03.2018');
INSERT INTO Configuration VALUES (2, 'Die Mobiliar', 'Bundesstrasse', '3000', 'Bern', '7463-9352-927', '18.09.2018');
INSERT INTO Configuration VALUES (3, 'Der Bund', 'Musterstrasse', '3000', 'Bern', '74634-9474-8364', '21.04.2019');
INSERT INTO Configuration VALUES (4, 'Garaio AG', 'Portugal-Strasse', '3000', 'Bern', '1954-0319-4194', '01.01.2021');
INSERT INTO Configuration VALUES (5, 'SBB', 'GA-Strasse', '3014', 'Bern', 'AB54-GHEJ-84F8', '12.06.2017');

-- Portfolio-Tabelle bef�llen mit Testdaten --
INSERT INTO Portfolio VALUES (1, 1, 'Marketing', 'Werbung, Kampagnen und Messen', 'Aktiv');
INSERT INTO Portfolio VALUES (2, 1, 'Sales', 'Eink�ufe, Verk�ufe und Kennzahlen', 'Aktiv');
INSERT INTO Portfolio VALUES (3, 1, 'ICT', 'interne Angelegenheiten', 'Inaktiv');

-- Customer-Tabelle bef�llen mit Testdaten --
INSERT INTO Customer VALUES (1, 'Darth', 'Vader', 'Todesstern 3', '123 456 83 94', 'dark_side@gmail.com', 'Star Wars', 'Weltall', '65748');
INSERT INTO Customer VALUES (2, 'Luke', 'Skywalker', 'Valhalla 73', '985 846 09 76', 'bright_side@gmail.com', 'Star Wars2', 'Universum', '7574');

-- ProjectStatus-Tabelle bef�llen mit Testdaten --
INSERT INTO ProjectStatus VALUES (1, 'Gr�n');
INSERT INTO ProjectStatus VALUES (2, 'Gelb');
INSERT INTO ProjectStatus VALUES (3, 'Rot');

-- Employee-Tabelle bef�llen mit Testdaten --
INSERT INTO Employee VALUES(1, 'Tobias', 'Stern', 'Business', 'PM', 160.00);
INSERT INTO Employee VALUES(2, 'Tobias', 'L�thi', 'IT', 'Applikationsentwickler', 150.00);
INSERT INTO Employee VALUES(3, 'Tenud', 'Chris', 'IT', 'Applikationsentwickler', 150.00);
INSERT INTO Employee VALUES(4, 'Omeni', 'Kim', 'HR', 'Personalmanagement', 145.00);
INSERT INTO Employee VALUES(5, 'Zimmermann', 'Nadja', 'Business', 'Marketing', 155.00);

-- Risk-Tabelle bef�llen mit Testdaten --
INSERT INTO Riskclass VALUES(1, 1, 'Level 1');
INSERT INTO Riskclass VALUES(2, 2, 'Level 2');
INSERT INTO Riskclass VALUES(3, 3, 'Level 3');
INSERT INTO Riskclass VALUES(4, 4, 'Level 4');
INSERT INTO Riskclass VALUES(5, 5, 'Level 5');

-- Project-Tabelle bef�llen mit Testdaten --
INSERT INTO Project VALUES (1, 1, 'Boss-Pet', 'Kyle', 'Initialisation', 80000, 2000, 500, 500, 'MS1.1', '2017-03-01 07:00:00.000000000', '2017-09-01 07:00:00.000000000', 'Elmoooooo', 'Ertes erfasstes Projekt', 2, 1, 1, 1);
INSERT INTO Project VALUES (2, 1, 'TooExpensiveProject', 'Butters', 'Realisation', 50000, 30000, 10000, 10000, 'MS4.3', '2017-04-01 18:00:00.000000000', '2017-04-30 15:00:00.000000000', 'Logan', 'Projekt um Sales-Kennzahlen einzuf�hren', 1, 2, 1, 5);
INSERT INTO Project VALUES (3, 1, 'Kiwi', 'Cartman', 'Konzeption', 5000, 1000, 600, 200, 'MS3.7', '2017-01-01 07:00:00.000000000', '2017-11-01 18:00:00.000000000', 'John Wick', 'Neues Verfahren Berechnung Profit', 1, 2, 1, 3);
INSERT INTO Project VALUES (4, 1, 'Banane', 'Kenny', 'Einf�hrung', 25000, 13000, 15000, 7000, 'MS5.4', '2017-02-01 07:00:00.000000000', '2017-12-30 07:00:00.000000000', 'Peter', 'Integration des Frameworks SAFe', 2, 1, 1, 4);
INSERT INTO Project VALUES (5, 1, 'Orange', 'Stan', 'Planung', 45000, 12000, 8000, 6500, 'MS2.4', '2016-09-01 07:00:00.000000000', '2018-03-01 07:00:00.000000000', 'Rob', 'Migration Platform AIX auf Linux', 2, 1, 1, 2);

-- Project_employee-Tabelle bef�llen mit Testdaten --
INSERT INTO Project_employee VALUES(1, 5, 2);
INSERT INTO Project_employee VALUES(2, 1, 2);
INSERT INTO Project_employee VALUES(3, 2, 2);
INSERT INTO Project_employee VALUES(4, 1, 5);
INSERT INTO Project_employee VALUES(5, 2, 5);
INSERT INTO Project_employee VALUES(6, 3, 5);
INSERT INTO Project_employee VALUES(7, 4, 3);
INSERT INTO Project_employee VALUES(8, 5, 3);
INSERT INTO Project_employee VALUES(9, 1, 3);
