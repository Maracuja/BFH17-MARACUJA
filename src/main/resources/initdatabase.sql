-- -----------------------------------------------------
-- Schema ppm
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS ppm ;
DROP ALL OBJECTS;

-- -----------------------------------------------------
-- Table Customer
-- -----------------------------------------------------
DROP TABLE IF EXISTS Customer ;

CREATE TABLE IF NOT EXISTS Customer (
  pk_idCustomer INT NOT NULL AUTO_INCREMENT,
  Name VARCHAR(60) NULL,
  Firstname VARCHAR(60) NULL,
  Adress VARCHAR(80) NULL,
  Phone VARCHAR(45) NULL,
  Email VARCHAR(45) NULL,
  Company VARCHAR(45) NULL,
  State VARCHAR(45) NULL,
  ZIPCode VARCHAR(45) NULL,
  PRIMARY KEY (pk_idCustomer))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Configuration
-- -----------------------------------------------------
DROP TABLE IF EXISTS Configuration ;

CREATE TABLE IF NOT EXISTS Configuration (
  pk_idConfiguration INT NOT NULL AUTO_INCREMENT,
  Companyname VARCHAR(45) NULL,
  Adress VARCHAR(45) NULL,
  ZIPCode VARCHAR(45) NULL,
  State VARCHAR(45) NULL,
  Licencekey VARCHAR(45) NULL,
  ExpiryDate TIMESTAMP NULL,
  PRIMARY KEY (pk_idConfiguration))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table Portfolio
-- -----------------------------------------------------
DROP TABLE IF EXISTS Portfolio ;

CREATE TABLE IF NOT EXISTS Portfolio (
  pk_idPortfolio INT NOT NULL AUTO_INCREMENT,
  IsActive INT NULL,
  Name VARCHAR(100) NULL,
  Description VARCHAR(255) NULL,
  PRIMARY KEY (pk_idPortfolio)
  )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table Riskclass
-- -----------------------------------------------------
DROP TABLE IF EXISTS Riskclass ;

CREATE TABLE IF NOT EXISTS Riskclass (
  pk_idRisk INT NOT NULL AUTO_INCREMENT,
  Class INT NULL,
  Text VARCHAR(255) NULL,
  PRIMARY KEY (pk_idRisk))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table ProjectPhase
-- -----------------------------------------------------
DROP TABLE IF EXISTS ProjectPhase ;

CREATE TABLE IF NOT EXISTS ProjectPhase (
  pk_idProjectPhase INT NOT NULL AUTO_INCREMENT,
  Phase VARCHAR(255) NULL,
  PRIMARY KEY (pk_idProjectPhase))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table Project
-- -----------------------------------------------------
DROP TABLE IF EXISTS Project ;

CREATE TABLE IF NOT EXISTS Project (
  pk_idProject INT NOT NULL AUTO_INCREMENT,
  IsActive INT NULL,
  Name VARCHAR(45) NULL,
  Owner VARCHAR(45) NULL,
  Budget INT NULL,
  PlannedValue INT NULL,
  ActualCost INT NULL,
  Milestones VARCHAR(45) NULL,
  Startdate TIMESTAMP NULL,
  Enddate TIMESTAMP NULL,
  Stakeholder VARCHAR(60) NULL,
  Description VARCHAR(255) NULL,
  fk_Customer INT NULL,
  fk_Portfolio INT NULL,
  fk_RiskClass INT NULL,
  fk_ProjectPhase INT NULL,
  PRIMARY KEY (pk_idProject),
  CONSTRAINT fk_customer
    FOREIGN KEY (fk_Customer)
    REFERENCES Customer (pk_idCustomer),
  CONSTRAINT fk_Portfolio
    FOREIGN KEY (fk_Portfolio)
    REFERENCES Portfolio (pk_idPortfolio),
  CONSTRAINT fk_RiskClass
    FOREIGN KEY (fk_RiskClass)
    REFERENCES Riskclass (pk_idRisk),
  CONSTRAINT fk_ProjectPhase
    FOREIGN KEY (fk_ProjectPhase)
    REFERENCES ProjectPhase (pk_idProjectPhase)
    )
ENGINE = InnoDB;

CREATE INDEX fk_customer_idx ON Project (fk_Customer ASC);

CREATE INDEX fk_Portfolio_idx ON Project (fk_Portfolio ASC);

CREATE INDEX fk_riskClass_idx ON Project (fk_RiskClass ASC);

CREATE INDEX fk_ProjectPhase_idx ON Project (fk_ProjectPhase ASC);


-- -----------------------------------------------------
-- Table Employee
-- -----------------------------------------------------
DROP TABLE IF EXISTS Employee ;

CREATE TABLE IF NOT EXISTS Employee (
  pk_idEmployee INT NOT NULL AUTO_INCREMENT,
  Firstname VARCHAR(45) NULL,
  Lastname VARCHAR(45) NULL,
  Section VARCHAR(45) NULL,
  Function VARCHAR(45) NULL,
  Rate DECIMAL(5,2) NULL,
  PRIMARY KEY (pk_idEmployee))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table Project_employee
-- -----------------------------------------------------
DROP TABLE IF EXISTS Project_employee ;

CREATE TABLE IF NOT EXISTS Project_employee (
  pk_idProjectEmployee INT NOT NULL AUTO_INCREMENT,
  fk_Employee INT NULL,
  fk_Project INT NULL,
  PRIMARY KEY (pk_idProjectEmployee),
  CONSTRAINT fk_Employee
    FOREIGN KEY (fk_Employee)
    REFERENCES Employee (pk_idEmployee),
  CONSTRAINT fk_Project
    FOREIGN KEY (fk_Project)
    REFERENCES Project (pk_idProject)
    )
ENGINE = InnoDB;

CREATE INDEX fk_Employee_idx ON Project_employee (fk_Employee ASC);

CREATE INDEX fk_Project_idx ON Project_employee (fk_Project ASC);

-- Testdaten --
-- Configuration-Tabelle bef�llen mit Testdaten --
INSERT INTO Configuration VALUES (1, 'RUAG', 'Together-Strasse', '3000', 'Bern', '1234-5678-0936', TO_TIMESTAMP('2017-03-01 07:00:00.000000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO Configuration VALUES (2, 'Die Mobiliar', 'Bundesstrasse', '3000', 'Bern', '7463-9352-927', TO_TIMESTAMP('2017-03-01 07:00:00.000000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO Configuration VALUES (3, 'Der Bund', 'Musterstrasse', '3000', 'Bern', '74634-9474-8364', TO_TIMESTAMP('2017-03-01 07:00:00.000000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO Configuration VALUES (4, 'Garaio AG', 'Portugal-Strasse', '3000', 'Bern', '1954-0319-4194', TO_TIMESTAMP('2017-03-01 07:00:00.000000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO Configuration VALUES (5, 'SBB', 'GA-Strasse', '3014', 'Bern', 'AB54-GHEJ-84F8', TO_TIMESTAMP('2017-03-01 07:00:00.000000000', 'YYYY-MM-DD HH24:MI:SS.FF'));

-- Portfolio-Tabelle bef�llen mit Testdaten --
INSERT INTO Portfolio VALUES (1, 1, 'Marketing', 'Werbung, Kampagnen und Messen');
INSERT INTO Portfolio VALUES (2, 1, 'Sales', 'Eink�ufe, Verk�ufe und Kennzahlen');
INSERT INTO Portfolio VALUES (3, 1, 'ICT', 'interne Angelegenheiten');

-- Customer-Tabelle bef�llen mit Testdaten --
INSERT INTO Customer VALUES (1, 'Darth', 'Vader', 'Todesstern 3', '123 456 83 94', 'dark_side@gmail.com', 'Star Wars', 'Weltall', '65748');
INSERT INTO Customer VALUES (2, 'Luke', 'Skywalker', 'Valhalla 73', '985 846 09 76', 'bright_side@gmail.com', 'Star Wars2', 'Universum', '7574');

-- Employee-Tabelle bef�llen mit Testdaten --
INSERT INTO Employee VALUES(1, 'Stern', 'Tobias', 'Business', 'PM', 160.00);
INSERT INTO Employee VALUES(2, 'L�thi', 'Tobias', 'IT', 'Applikationsentwickler', 150.00);
INSERT INTO Employee VALUES(3, 'Chris', 'Tenud', 'IT', 'Applikationsentwickler', 150.00);
INSERT INTO Employee VALUES(4, 'Omeni', 'Kim', 'HR', 'Personalmanagement', 145.00);
INSERT INTO Employee VALUES(5, 'Zimmermann', 'Nadja', 'Business', 'Marketing', 155.00);
INSERT INTO Employee VALUES(6, 'Fankhauser', 'Alexandra', 'Junior IT Architektin', 'Consulting', 122.00);

-- Risk-Tabelle bef�llen mit Testdaten --
INSERT INTO Riskclass VALUES(1, 1, 'Level 1');
INSERT INTO Riskclass VALUES(2, 2, 'Level 2');
INSERT INTO Riskclass VALUES(3, 3, 'Level 3');
INSERT INTO Riskclass VALUES(4, 4, 'Level 4');
INSERT INTO Riskclass VALUES(5, 5, 'Level 5');

-- ProjectPhase-Tabelle bef�llen mit Testdaten --
INSERT INTO ProjectPhase VALUES(1, 'Initialisierung');
INSERT INTO ProjectPhase VALUES(2, 'Konzeption');
INSERT INTO ProjectPhase VALUES(3, 'Realisierung');
INSERT INTO ProjectPhase VALUES(4, 'Einf�hrung');
INSERT INTO ProjectPhase VALUES(5, 'Abschluss');
INSERT INTO ProjectPhase VALUES(6, 'Abgeschlossen');

-- Project-Tabelle bef�llen mit Testdaten --
INSERT INTO Project VALUES (1, 1, 'Boss-Pet', 'Kyle', 80000, 2000, 500, 'MS1.1', TO_TIMESTAMP('2017-03-01 07:00:00.000000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_TIMESTAMP('2017-11-01 07:00:00.000000000', 'YYYY-MM-DD HH24:MI:SS.FF'), 'Elmoooooo', 'Ertes erfasstes Projekt', 2, 1, 1,1);
INSERT INTO Project VALUES (2, 1, 'TooExpensiveProject', 'Realisation', 50000, 30000, 60000, 'MS4.3', TO_TIMESTAMP('2017-04-01 18:00:00.000000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_TIMESTAMP('2017-04-30 15:00:00.000000000', 'YYYY-MM-DD HH24:MI:SS.FF'), 'Logan', 'Projekt um Sales-Kennzahlen einzuf�hren', 1, 2, 5,2);
INSERT INTO Project VALUES (3, 1, 'Kiwi', 'Cartman', 5000, 1000, 600, 'MS3.7', TO_TIMESTAMP('2017-01-01 07:00:00.000000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_TIMESTAMP('2018-11-01 18:00:00.000000000', 'YYYY-MM-DD HH24:MI:SS.FF'), 'John Wick', 'Neues Verfahren Berechnung Profit', 1, 2, 3,3);
INSERT INTO Project VALUES (4, 1, 'Banane', 'Kenny', 25000, 13000, 15000, 'MS5.4', TO_TIMESTAMP('2017-02-01 07:00:00.000000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_TIMESTAMP('2018-12-30 07:00:00.000000000', 'YYYY-MM-DD HH24:MI:SS.FF'), 'Peter', 'Integration des Frameworks SAFe', 2, 1, 4,4);
INSERT INTO Project VALUES (5, 1, 'Orange', 'Stan', 45000, 12000, 8000, 'MS2.4', TO_TIMESTAMP('2016-09-01 07:00:00.000000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_TIMESTAMP('2018-03-01 07:00:00.000000000', 'YYYY-MM-DD HH24:MI:SS.FF'), 'Rob', 'Migration Platform AIX auf Linux', 2, 1, 2,6);

-- Project_employee-Tabelle bef�llen mit Testdaten --
INSERT INTO Project_employee VALUES(1, 5, 2);
INSERT INTO Project_employee VALUES(2, 1, 2);
INSERT INTO Project_employee VALUES(3, 2, 2);
INSERT INTO Project_employee VALUES(4, 1, 5);
INSERT INTO Project_employee VALUES(5, 2, 5);
INSERT INTO Project_employee VALUES(6, 3, 5);
INSERT INTO Project_employee VALUES(7, 4, 3);
INSERT INTO Project_employee VALUES(8, 5, 3);
INSERT INTO Project_employee VALUES(9, 1, 3);
