 var editor;
 
$(document).ready(function() {
    var customerTable = $('#customer').DataTable( {
        ajax: "/api/project/customer/1",
        sAjaxDataProp: "",
        select: true,
        columns: [
            { data: "id" },
            { data: "name" },
            { data: "firstname" },
            { data: "phone" },
            { data: "email" },
            { data: "company" },
            { data: "adress" },
            { data: "zipCode" },
            { data: "state" }
        ],
        select: true
    } );
    
    var employeeTable = $('#employee').DataTable( {
        ajax: "/api/project/employee/1",
        sAjaxDataProp: "",
        select: true,
        columns: [
            { data: "id" },
            { data: "lastname" },
            { data: "firstname" },
            { data: "section" },
            { data: "function" },
            { data: "rate", render: $.fn.dataTable.render.number( "'", '.', 2, '', ' CHF' )  }
        ],
        select: true
    } );
    
    employeeTable.on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
        	employeeTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            var data = employeeTable.rows( '.selected' ).data();
            $('.ui.modal.employee.' + data[0]["id"]).modal('show');
        }
    } );
    
    customerTable.on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
        	customerTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            var data = customerTable.rows( '.selected' ).data();
            $('.ui.modal.customer.' + data[0]["id"]).modal('show');
        }
    } );
    
    
    $( "#addEmployee" ).on( "click", function() {
        $('.ui.modal.employee.empty').modal('show');
    });  
    
    $( "#addCustomer" ).on( "click", function() {
        $('.ui.modal.customer.empty').modal('show');
    });
    
} );