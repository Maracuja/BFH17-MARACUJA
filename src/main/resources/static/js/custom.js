$('input[id=nbredit_budget]').change(calculateEarnedValue);
$('input[id=startDate]').change(calculateEarnedValue);
$('input[id=endDate]').change(calculateEarnedValue);

var error = false;
var error2 = false;
$( document ).ready(function() {
	$(":input[id^='textedit']").each( function() {
		$("#"+this.id+"_error").hide();	
	});
	$(":input[id^='nbredit']").each( function() {
		$("#"+this.id+"_error").hide();	
	});
});

function calculateEarnedValue() {
	var value = 0;
	var budget = $('input[id=nbredit_budget]').val();
	var start = new Date($('input[id=startDate]').val());
	var end = new Date($('input[id=endDate]').val());
	var today = new Date();
	
	if(end <=today){
		value = budget;
	}
	else if(start >=today){
		value = 0
	}
	else {
		var q = Math.abs(today-start);
		var d = Math.abs(end-start);
		var progress = q/d;	
		value = (Math.round(budget*progress * 1) / 1);
	}
	$('input[id=nbredit_eV]').val(value);
}

/* Text-Validation: id des Feldes muss mit textedit_ beginnen */
/* Nummerische Validation: id des Feldes muss mit nbredit_ beginnen */
function validateNotEmpty(str, id) {
	if(str.length === 0 || !str.trim()) {
		$("#"+id+"").addClass("error_class");
		$("#"+id+"_error").show();	
	} else {
		error = false;
		$("#"+id+"").removeClass("error_class");
		$("#"+id+"_error").hide();	
		inputTextError();
	}
}

function validateNumbers(nbr, id) {
	if(!$.isNumeric(nbr)) {
		$("#"+id+"").addClass("error_class");
		$("#"+id+"_error").show();	
	} else {
		error2 = false;
		$("#"+id+"").removeClass("error_class");
		$("#"+id+"_error").hide();	
		inputNbrError();
	}
}

$("[id^='form_send']").on("keyup keypress", function(e) {
	var keyCode = e.keyCode || e.which;
	if(keyCode === 13 && error == true && error2 == true) {
		e.preventDefault();
		return false;
	}
});

$("[id^='config_btn_submit']").click(function () {
	error = false;
	error2 = false;
	inputTextError();
	inputNbrError();
	if (error || error2) {
		return false;
	}
});

$("[id^='btn_submit']").click(function () {
	validateText();
	validateNumber();
	error = false;
	error2 = false;
	inputTextError();
	inputNbrError();
	if (error || error2) {
		return false;
	}
});

//Set error on all empty text inputs
function inputTextError() {
	$(":input[id^='textedit']").each( function() {
	    if ($("#"+this.id+"").hasClass("error_class")) {
	    	$("#"+this.id+"_error").show();	
	    	error = true;
	    } else {
	    	$("#"+this.id+"_error").hide();	
	    }
	});
}

//Set error on all empty nbr inputs
function inputNbrError() {
	$(":input[id^='nbredit']").each( function() {
	    if ($("#"+this.id+"").hasClass("error_class")) {
	    	$("#"+this.id+"_error").show();	
	    	error2 = true;
	    } else {
	    	$("#"+this.id+"_error").hide();	
	    }
	});
}

function validateText() {
	$(":input[id^='textedit']").each( function(){
		var idName = '';
		idName = this.id;
		if(idName.trim() !== '') {
			var string = $("#"+idName+"").val();
			validateNotEmpty(string, idName);
		}
	});	
}

function validateNumber() {
	$(":input[id^='nbredit']").each( function() {
		var idName = '';
		idName = this.id;
		if(idName.trim() !== '') {
			var nbr = $("#"+idName+"").val();
			validateNumbers(nbr, idName);
		}
	});
}

//Validation of all the inputs with text
$(":input[id^='textedit']").on("change keyup paste click", function(){
	
	var idName = '';
	$(":focus").each(function() {
		idName = this.id;
	});
	if(idName.trim() !== '') {
		var string = $("#"+idName+"").val();
		validateNotEmpty(string, idName);
	} //else focus lost
});	

//Validation of all the numeric fields
$(":input[id^='nbredit']").on("change keyup paste click", function() {
	var idName = '';
	$(":focus").each(function() {
		idName = this.id;
	});
	if(idName.trim() !== '') {
		var nbr = $("#"+idName+"").val();
		validateNumbers(nbr, idName);
	} //else lost focus
})

