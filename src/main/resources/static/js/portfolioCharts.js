var budgetctx = document.getElementById("budgetChart");
var id = getParameterByName("id");
var urlBudget = "/api/portfolio/budget/" + id;


$.getJSON( urlBudget, function( data ) {	
	var budgetData = {
		    labels: [
		        "Budget",
		        "Actual cost",
		    ],
		    datasets: [
		        {
				    label: false,
		            backgroundColor: [
		                "#22CECE",
		                "#FFCE56"
		            ],
		            borderColor: [
		                "#22CECE",
		                "#FFCE56"
		            ],
		            borderWidth: 1,
		            data: data
		        }]
		};

	var budgetChart = new Chart(budgetctx,{
	    type: 'bar',
	    stacked: true,
	    data: budgetData,   
	    options: {
	        animation:{
	            animateScale:true
	        },
	        legend: {
	            display: false,
	        },
			scales: {
		        yAxes: [{
		            ticks: {
		                beginAtZero:true
		            }
		        }]
		    }
	    }
	});
});


function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}