# Projektportfolio Plattform

## Intro

This project was created for the BFH module TOSE. It is a projectmanagment tool:
-	Thymeleaf, HTML,  CSS as view technologies
- 	H2 SQL as database
-	SparkJava as web framework
-	Chart.js and semantic-ui for the design

It has the following functionality:
-	Multiple portfolios
-	Multiple projects 
-	Risk and budget analyse
-	Summary over a project
-	Summary over a portfolio
-	User and Customer management
-	EV calculation

## Development


### Branching
- master: the main branch for the latest release